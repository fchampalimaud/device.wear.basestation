#include <avr/io.h>
#include "hwbp_core_types.h"
#include "app_ios_and_regs.h"

/************************************************************************/
/* Configure and initialize IOs                                         */
/************************************************************************/
void init_ios(void)
{	/* Configure input pins */
	io_pin2in(&PORTB, 1, PULL_IO_UP, SENSE_IO_EDGE_FALLING);             // SAMPLE_RF2
	io_pin2in(&PORTB, 2, PULL_IO_UP, SENSE_IO_EDGE_FALLING);             // SAMPLE_RF1
	io_pin2in(&PORTB, 3, PULL_IO_UP, SENSE_IO_EDGE_FALLING);             // SAMPLE_WIRED
	io_pin2in(&PORTC, 5, PULL_IO_TRISTATE, SENSE_IO_EDGES_BOTH);         // DI0
	io_pin2in(&PORTA, 7, PULL_IO_TRISTATE, SENSE_IO_EDGES_BOTH);         // DI1
	io_pin2in(&PORTD, 2, PULL_IO_UP, SENSE_IO_EDGE_FALLING);             // SW_START
	io_pin2in(&PORTB, 0, PULL_IO_UP, SENSE_IO_EDGE_FALLING);             // SW_STOP_SELECT

	/* Configure input interrupts */
	io_set_int(&PORTB, INT_LEVEL_LOW, 0, (1<<1), false);                 // SAMPLE_RF2
	io_set_int(&PORTB, INT_LEVEL_LOW, 0, (1<<2), false);                 // SAMPLE_RF1
	io_set_int(&PORTB, INT_LEVEL_LOW, 0, (1<<3), false);                 // SAMPLE_WIRED
	io_set_int(&PORTC, INT_LEVEL_LOW, 1, (1<<5), false);                 // DI0
	io_set_int(&PORTA, INT_LEVEL_LOW, 1, (1<<7), false);                 // DI1
	io_set_int(&PORTD, INT_LEVEL_LOW, 1, (1<<2), false);                 // SW_START
	io_set_int(&PORTB, INT_LEVEL_LOW, 1, (1<<0), false);                 // SW_STOP_SELECT

	/* Configure output pins */
	io_pin2out(&PORTA, 2, OUT_IO_DIGITAL, IN_EN_IO_DIS);                 // EN_RF2
	io_pin2out(&PORTA, 5, OUT_IO_DIGITAL, IN_EN_IO_DIS);                 // EN_RF1
	io_pin2out(&PORTC, 2, OUT_IO_DIGITAL, IN_EN_IO_DIS);                 // EN_WIRED
	io_pin2out(&PORTC, 3, OUT_IO_DIGITAL, IN_EN_IO_DIS);                 // UART_RF2
	io_pin2out(&PORTC, 1, OUT_IO_DIGITAL, IN_EN_IO_DIS);                 // UART_RF1
	io_pin2out(&PORTD, 1, OUT_IO_DIGITAL, IN_EN_IO_DIS);                 // UART_WIRED
	io_pin2out(&PORTD, 7, OUT_IO_DIGITAL, IN_EN_IO_DIS);                 // RST_RF1
	io_pin2out(&PORTC, 4, OUT_IO_DIGITAL, IN_EN_IO_EN);                  // DO0
	io_pin2out(&PORTA, 6, OUT_IO_DIGITAL, IN_EN_IO_EN);                  // DO1
	io_pin2out(&PORTD, 0, OUT_IO_DIGITAL, IN_EN_IO_EN);                  // CAM0
	io_pin2out(&PORTC, 0, OUT_IO_DIGITAL, IN_EN_IO_EN);                  // CAM1

	/* Initialize output pins */
	clr_EN_RF2;
	clr_EN_RF1;
	clr_EN_WIRED;
	clr_UART_RF2;
	clr_UART_RF1;
	clr_UART_WIRED;
	set_RST_RF1;
	clr_DO0;
	clr_DO1;
	clr_CAM0;
	clr_CAM1;
}

/************************************************************************/
/* Registers' stuff                                                     */
/************************************************************************/
AppRegs app_regs;

uint8_t app_regs_type[] = {
	TYPE_U8,
	TYPE_U8,
	TYPE_I16,
	TYPE_U16,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U16,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U16,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U16,
	TYPE_U16,
	TYPE_U16,
	TYPE_U16,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U16,
	TYPE_U16,
	TYPE_U16,
	TYPE_U8,
	TYPE_U8,
	TYPE_U16,
	TYPE_U16,
	TYPE_U16,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U16
};

uint16_t app_regs_n_elements[] = {
	1,
	1,
	10,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1
};

uint8_t *app_regs_pointer[] = {
	(uint8_t*)(&app_regs.REG_START_DATA),
	(uint8_t*)(&app_regs.REG_START_STIM),
	(uint8_t*)(app_regs.REG_DATA),
	(uint8_t*)(&app_regs.REG_MISC),
	(uint8_t*)(&app_regs.REG_CAM0),
	(uint8_t*)(&app_regs.REG_CAM1),
	(uint8_t*)(&app_regs.REG_DOUT0),
	(uint8_t*)(&app_regs.REG_DOUT1),
	(uint8_t*)(&app_regs.REG_ACQ_STATUS),
	(uint8_t*)(&app_regs.REG_ACQ_EXT_TRIGGER),
	(uint8_t*)(&app_regs.REG_DEV_SELECT),
	(uint8_t*)(&app_regs.REG_TEMP),
	(uint8_t*)(&app_regs.REG_TX_RETRIES),
	(uint8_t*)(&app_regs.REG_BATTERY),
	(uint8_t*)(&app_regs.REG_BATTERY_RAW),
	(uint8_t*)(&app_regs.REG_WEAR_FW_MAJOR),
	(uint8_t*)(&app_regs.REG_WEAR_FW_MINOR),
	(uint8_t*)(&app_regs.REG_WEAR_HW_MAJOR),
	(uint8_t*)(&app_regs.REG_WEAR_HW_MINOR),
	(uint8_t*)(&app_regs.REG_RECEIVER_FW_MAJOR),
	(uint8_t*)(&app_regs.REG_RECEIVER_FW_MINOR),
	(uint8_t*)(&app_regs.REG_RECEIVER_HW_MAJOR),
	(uint8_t*)(&app_regs.REG_RECEIVER_HW_MINOR),
	(uint8_t*)(&app_regs.REG_RX_GOOD),
	(uint8_t*)(&app_regs.REG_RESERVED3),
	(uint8_t*)(&app_regs.REG_CONF_DEV_ID),
	(uint8_t*)(&app_regs.REG_CONF_SAMPLE_FREQ),
	(uint8_t*)(&app_regs.REG_CONF_RANGE_ACC),
	(uint8_t*)(&app_regs.REG_CONF_RANGE_GYR),
	(uint8_t*)(&app_regs.REG_CONF_ENABLE),
	(uint8_t*)(&app_regs.REG_CONF_PRIORITIZATION),
	(uint8_t*)(&app_regs.REG_CONF_IR_LEDS_PERIOD),
	(uint8_t*)(&app_regs.REG_CONF_TX_POWER),
	(uint8_t*)(&app_regs.REG_RESERVED4),
	(uint8_t*)(&app_regs.REG_RESERVED5),
	(uint8_t*)(&app_regs.REG_STIM_FROM_DI0),
	(uint8_t*)(&app_regs.REG_STIM_ON),
	(uint8_t*)(&app_regs.REG_STIM_OFF),
	(uint8_t*)(&app_regs.REG_STIM_REPS),
	(uint8_t*)(&app_regs.REG_STIM_CURRENT),
	(uint8_t*)(&app_regs.REG_RESERVED6),
	(uint8_t*)(&app_regs.REG_RESERVED7),
	(uint8_t*)(&app_regs.REG_MISC_CONF_DI),
	(uint8_t*)(&app_regs.REG_MISC_CONF_RUN),
	(uint8_t*)(&app_regs.REG_CAM0_CONF),
	(uint8_t*)(&app_regs.REG_CAM0_CONF_RUN),
	(uint8_t*)(&app_regs.REG_CAM0_FREQ),
	(uint8_t*)(&app_regs.REG_CAM0_MMODE_PERIOD),
	(uint8_t*)(&app_regs.REG_CAM0_MMODE_PULSE),
	(uint8_t*)(&app_regs.REG_CAM1_CONF),
	(uint8_t*)(&app_regs.REG_CAM1_CONF_RUN),
	(uint8_t*)(&app_regs.REG_CAM1_FREQ),
	(uint8_t*)(&app_regs.REG_CAM1_MMODE_PERIOD),
	(uint8_t*)(&app_regs.REG_CAM1_MMODE_PULSE),
	(uint8_t*)(&app_regs.REG_DO0_CONF),
	(uint8_t*)(&app_regs.REG_DO1_CONF),
	(uint8_t*)(&app_regs.REG_DO1_PULSE),
	(uint8_t*)(&app_regs.REG_RESERVED8),
	(uint8_t*)(&app_regs.REG_RESERVED9),
	(uint8_t*)(&app_regs.REG_EVNT_ENABLE)
};