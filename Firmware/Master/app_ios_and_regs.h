#ifndef _APP_IOS_AND_REGS_H_
#define _APP_IOS_AND_REGS_H_
#include "cpu.h"

void init_ios(void);
/************************************************************************/
/* Definition of input pins                                             */
/************************************************************************/
// SAMPLE_RF2             Description: Sample from RF2
// SAMPLE_RF1             Description: Sample from RF1
// SAMPLE_WIRED           Description: Sample from Wired
// DI0                    Description: Digital input DI0
// DI1                    Description: Digital input DI1
// SW_START               Description: Switch to select and stop
// SW_STOP_SELECT         Description: Switch to select and stop

#define read_SAMPLE_RF2 read_io(PORTB, 1)       // SAMPLE_RF2
#define read_SAMPLE_RF1 read_io(PORTB, 2)       // SAMPLE_RF1
#define read_SAMPLE_WIRED read_io(PORTB, 3)     // SAMPLE_WIRED
#define read_DI0 read_io(PORTC, 5)              // DI0
#define read_DI1 read_io(PORTA, 7)              // DI1
#define read_SW_START read_io(PORTD, 2)         // SW_START
#define read_SW_STOP_SELECT read_io(PORTB, 0)   // SW_STOP_SELECT

/************************************************************************/
/* Definition of output pins                                            */
/************************************************************************/
// EN_RF2                 Description: Enable RF2 circuitry
// EN_RF1                 Description: Enable RF1 circuitry
// EN_WIRED               Description: Enable Wired circuitry
// UART_RF2               Description: UART enbale for RF2
// UART_RF1               Description: UART enbale for RF1
// UART_WIRED             Description: UART enbale for WIRED
// RST_RF1                Description: UART enbale for WIRED
// DO0                    Description: Digital output DO0
// DO1                    Description: Digital output DO1
// CAM0                   Description: Camera control 0
// CAM1                   Description: Camera control 1

/* EN_RF2 */
#define set_EN_RF2 clear_io(PORTA, 2)
#define clr_EN_RF2 set_io(PORTA, 2)
#define tgl_EN_RF2 toggle_io(PORTA, 2)
#define read_EN_RF2 read_io(PORTA, 2)

/* EN_RF1 */
#define set_EN_RF1 set_io(PORTA, 5)
#define clr_EN_RF1 clear_io(PORTA, 5)
#define tgl_EN_RF1 toggle_io(PORTA, 5)
#define read_EN_RF1 read_io(PORTA, 5)

/* EN_WIRED */
#define set_EN_WIRED clear_io(PORTC, 2)
#define clr_EN_WIRED set_io(PORTC, 2)
#define tgl_EN_WIRED toggle_io(PORTC, 2)
#define read_EN_WIRED read_io(PORTC, 2)

/* UART_RF2 */
#define set_UART_RF2 clear_io(PORTC, 3)
#define clr_UART_RF2 set_io(PORTC, 3)
#define tgl_UART_RF2 toggle_io(PORTC, 3)
#define read_UART_RF2 read_io(PORTC, 3)

/* UART_RF1 */
#define set_UART_RF1 clear_io(PORTC, 1)
#define clr_UART_RF1 set_io(PORTC, 1)
#define tgl_UART_RF1 toggle_io(PORTC, 1)
#define read_UART_RF1 read_io(PORTC, 1)

/* UART_WIRED */
#define set_UART_WIRED clear_io(PORTD, 1)
#define clr_UART_WIRED set_io(PORTD, 1)
#define tgl_UART_WIRED toggle_io(PORTD, 1)
#define read_UART_WIRED read_io(PORTD, 1)

/* RST_RF1 */
#define set_RST_RF1 set_io(PORTD, 7)
#define clr_RST_RF1 clear_io(PORTD, 7)
#define tgl_RST_RF1 toggle_io(PORTD, 7)
#define read_RST_RF1 read_io(PORTD, 7)

/* DO0 */
#define set_DO0 set_io(PORTC, 4)
#define clr_DO0 clear_io(PORTC, 4)
#define tgl_DO0 toggle_io(PORTC, 4)
#define read_DO0 read_io(PORTC, 4)

/* DO1 */
#define set_DO1 set_io(PORTA, 6)
#define clr_DO1 clear_io(PORTA, 6)
#define tgl_DO1 toggle_io(PORTA, 6)
#define read_DO1 read_io(PORTA, 6)

/* CAM0 */
#define set_CAM0 set_io(PORTD, 0)
#define clr_CAM0 clear_io(PORTD, 0)
#define tgl_CAM0 toggle_io(PORTD, 0)
#define read_CAM0 read_io(PORTD, 0)

/* CAM1 */
#define set_CAM1 set_io(PORTC, 0)
#define clr_CAM1 clear_io(PORTC, 0)
#define tgl_CAM1 toggle_io(PORTC, 0)
#define read_CAM1 read_io(PORTC, 0)


/************************************************************************/
/* Registers' structure                                                 */
/************************************************************************/
typedef struct
{
	uint8_t REG_START_DATA;
	uint8_t REG_START_STIM;
	int16_t REG_DATA[10];
	uint16_t REG_MISC;
	uint8_t REG_CAM0;
	uint8_t REG_CAM1;
	uint8_t REG_DOUT0;
	uint8_t REG_DOUT1;
	uint8_t REG_ACQ_STATUS;
	uint8_t REG_ACQ_EXT_TRIGGER;
	uint8_t REG_DEV_SELECT;
	uint8_t REG_TEMP;
	uint16_t REG_TX_RETRIES;
	uint8_t REG_BATTERY;
	uint8_t REG_BATTERY_RAW;
	uint8_t REG_WEAR_FW_MAJOR;
	uint8_t REG_WEAR_FW_MINOR;
	uint8_t REG_WEAR_HW_MAJOR;
	uint8_t REG_WEAR_HW_MINOR;
	uint8_t REG_RECEIVER_FW_MAJOR;
	uint8_t REG_RECEIVER_FW_MINOR;
	uint8_t REG_RECEIVER_HW_MAJOR;
	uint8_t REG_RECEIVER_HW_MINOR;
	uint8_t REG_RX_GOOD;
	uint8_t REG_RESERVED3;
	uint16_t REG_CONF_DEV_ID;
	uint8_t REG_CONF_SAMPLE_FREQ;
	uint8_t REG_CONF_RANGE_ACC;
	uint8_t REG_CONF_RANGE_GYR;
	uint8_t REG_CONF_ENABLE;
	uint8_t REG_CONF_PRIORITIZATION;
	uint8_t REG_CONF_IR_LEDS_PERIOD;
	uint8_t REG_CONF_TX_POWER;
	uint8_t REG_RESERVED4;
	uint8_t REG_RESERVED5;
	uint8_t REG_STIM_FROM_DI0;
	uint16_t REG_STIM_ON;
	uint16_t REG_STIM_OFF;
	uint16_t REG_STIM_REPS;
	uint16_t REG_STIM_CURRENT;
	uint8_t REG_RESERVED6;
	uint8_t REG_RESERVED7;
	uint8_t REG_MISC_CONF_DI;
	uint8_t REG_MISC_CONF_RUN;
	uint8_t REG_CAM0_CONF;
	uint8_t REG_CAM0_CONF_RUN;
	uint16_t REG_CAM0_FREQ;
	uint16_t REG_CAM0_MMODE_PERIOD;
	uint16_t REG_CAM0_MMODE_PULSE;
	uint8_t REG_CAM1_CONF;
	uint8_t REG_CAM1_CONF_RUN;
	uint16_t REG_CAM1_FREQ;
	uint16_t REG_CAM1_MMODE_PERIOD;
	uint16_t REG_CAM1_MMODE_PULSE;
	uint8_t REG_DO0_CONF;
	uint8_t REG_DO1_CONF;
	uint8_t REG_DO1_PULSE;
	uint8_t REG_RESERVED8;
	uint8_t REG_RESERVED9;
	uint16_t REG_EVNT_ENABLE;
} AppRegs;

/************************************************************************/
/* Registers' address                                                   */
/************************************************************************/
/* Registers */
#define ADD_REG_START_DATA                  32 // U8     
#define ADD_REG_START_STIM                  33 // U8     
#define ADD_REG_DATA                        34 // I16    Data from the sensors (9 axis)
#define ADD_REG_MISC                        35 // U16    Value of both digital inputs and analog input
#define ADD_REG_CAM0                        36 // U8     Camera 0's triggering
#define ADD_REG_CAM1                        37 // U8     Camera 1's triggering
#define ADD_REG_DOUT0                       38 // U8     Write to digital output DO0
#define ADD_REG_DOUT1                       39 // U8     Write to digital output DO1
#define ADD_REG_ACQ_STATUS                  40 // U8     Contains the status of que acquisition (LSB equal to 1 if acquisirng)
#define ADD_REG_ACQ_EXT_TRIGGER             41 // U8     Configures the Start and Stop using an external trigger
#define ADD_REG_DEV_SELECT                  42 // U8     Select the device to use
#define ADD_REG_TEMP                        43 // U8     Temperature of the sensors
#define ADD_REG_TX_RETRIES                  44 // U16    Number of retransmitions of the last second
#define ADD_REG_BATTERY                     45 // U8     Percentage of device's battery
#define ADD_REG_BATTERY_RAW                 46 // U8     Raw percentage of device's battery (use for for debug only)
#define ADD_REG_WEAR_FW_MAJOR               47 // U8     Firmware version of the WEAR device (major part)
#define ADD_REG_WEAR_FW_MINOR               48 // U8     Firmware version of the WEAR device (minor part)
#define ADD_REG_WEAR_HW_MAJOR               49 // U8     Hardware version of the WEAR device (major part)
#define ADD_REG_WEAR_HW_MINOR               50 // U8     Hardware version of the WEAR device (minor part)
#define ADD_REG_RECEIVER_FW_MAJOR           51 // U8     Firmware version of the receiver device (major part)
#define ADD_REG_RECEIVER_FW_MINOR           52 // U8     Firmware version of the receiver device (minor part)
#define ADD_REG_RECEIVER_HW_MAJOR           53 // U8     Hardware version of the receiver device (major part)
#define ADD_REG_RECEIVER_HW_MINOR           54 // U8     Hardware version of the receiver device (minor part)
#define ADD_REG_RX_GOOD                     55 // U8     Power on the receiver above -64 dBm
#define ADD_REG_RESERVED3                   56 // U8     Reserved for possible future use
#define ADD_REG_CONF_DEV_ID                 57 // U16    Id of the WEAR device [0;8191]
#define ADD_REG_CONF_SAMPLE_FREQ            58 // U8     Configure the sensors' sample frequency
#define ADD_REG_CONF_RANGE_ACC              59 // U8     Configure the range of the accelerometer
#define ADD_REG_CONF_RANGE_GYR              60 // U8     Configure the range of the gyroscope
#define ADD_REG_CONF_ENABLE                 61 // U8     Enable of the sensors
#define ADD_REG_CONF_PRIORITIZATION         62 // U8     Configure what the device should prioritize
#define ADD_REG_CONF_IR_LEDS_PERIOD         63 // U8     Configure the period that each infrared LEDs will be ON
#define ADD_REG_CONF_TX_POWER               64 // U8     Configure the TX power used by the device
#define ADD_REG_RESERVED4                   65 // U8     Reserved for possible future use
#define ADD_REG_RESERVED5                   66 // U8     Reserved for possible future use
#define ADD_REG_STIM_FROM_DI0               67 // U8     Start stimulation on a digital input transition
#define ADD_REG_STIM_ON                     68 // U16    Amount of time that the STIM LED will be ON (in miliseconds) [1;60000]
#define ADD_REG_STIM_OFF                    69 // U16    Amount of time that the STIM LED will be OFF (in miliseconds) [1;60000]
#define ADD_REG_STIM_REPS                   70 // U16    Number of repetitions [1;60000]
#define ADD_REG_STIM_CURRENT                71 // U16    Current in milliamps [10;500]
#define ADD_REG_RESERVED6                   72 // U8     Reserved for possible future use
#define ADD_REG_RESERVED7                   73 // U8     Reserved for possible future use
#define ADD_REG_MISC_CONF_DI                74 // U8     Configures how Digital inputs will trigger a MISC read
#define ADD_REG_MISC_CONF_RUN               75 // U8     Configure when the MISC read is active
#define ADD_REG_CAM0_CONF                   76 // U8     Configures when the camera is triggered
#define ADD_REG_CAM0_CONF_RUN               77 // U8     Configure when the CAM0 is being triggered
#define ADD_REG_CAM0_FREQ                   78 // U16    Configures the camera 0's sample frequency [1;600]
#define ADD_REG_CAM0_MMODE_PERIOD           79 // U16    Configures the servo motor period (us) when using motor controller mode (sensitive to 2 us)
#define ADD_REG_CAM0_MMODE_PULSE            80 // U16    Configures the servo motor pulse (us) when using motor controller mode (sensitive to 2 us)
#define ADD_REG_CAM1_CONF                   81 // U8     Configures when the camera is triggered
#define ADD_REG_CAM1_CONF_RUN               82 // U8     Configure when the CAM1 is being triggered
#define ADD_REG_CAM1_FREQ                   83 // U16    Configures the camera 1's sample frequency [1;600]
#define ADD_REG_CAM1_MMODE_PERIOD           84 // U16    Configures the servo motor period (us) when using motor controller mode (sensitive to 2 us)
#define ADD_REG_CAM1_MMODE_PULSE            85 // U16    Configures the servo motor pulse (us) when using motor controller mode (sensitive to 2 us)
#define ADD_REG_DO0_CONF                    86 // U8     Configures how the output DO0 is handled
#define ADD_REG_DO1_CONF                    87 // U8     Configures how the output DO1 is handled
#define ADD_REG_DO1_PULSE                   88 // U8     Configures the pulse duration on DO1 (ms)
#define ADD_REG_RESERVED8                   89 // U8     Reserved for possible future use
#define ADD_REG_RESERVED9                   90 // U8     Reserved for possible future use
#define ADD_REG_EVNT_ENABLE                 91 // U16    Enable the Events

/************************************************************************/
/* PWM Generator registers' memory limits                               */
/*                                                                      */
/* DON'T change the APP_REGS_ADD_MIN value !!!                          */
/* DON'T change these names !!!                                         */
/************************************************************************/
/* Memory limits */
#define APP_REGS_ADD_MIN                    0x20
#define APP_REGS_ADD_MAX                    0x5B
#define APP_NBYTES_OF_REG_BANK              93

/************************************************************************/
/* Registers' bits                                                      */
/************************************************************************/
#define B_START                            (1<<0)       // Starts the acquisition when write to 1 and stop when write to 0
#define B_STIM                             (1<<0)       // Starts the stimulation when writen to 1
#define MSK_ADC                            0x7FF        // Analog input (12 bits)
#define B_DI0                              (1<<14)      // Digital input 0
#define B_DI1                              (1<<15)      // Digital input 1
#define B_CAM0                             (1<<0)       // Camera 0 was triggered
#define B_CAM1                             (1<<0)       // Camera 1 was triggered
#define B_DO0                              (1<<0)       // Output DO0
#define B_DO1                              (1<<0)       // Output DO1
#define B_STATUS                           (1<<0)       // Equal to 1 if the sensor device is sending data
#define MSK_EXT_CONF                       0x03         // External Acquisition configuration
#define GM_EXT_NOT_USED                    (0<<0)       // External control of Aquisition not used
#define GM_EXT_DI0                         (1<<0)       // Acquisition controlled trough pin DI0
#define GM_EXT_DI1                         (2<<0)       // Acquisition controlled trough pin DI1
#define MSK_SEL                            (3<<0)       // 
#define GM_SEL_WIRED                       (0<<0)       // Select Wired
#define GM_SEL_RF1                         (1<<0)       // Select RF1
#define GM_SEL_RF2                         (2<<0)       // Select RF2
#define B_RX_GOOD                          (1<<0)       // 
#define MSK_SENSOR_FREQ                    (7<<0)       // 
#define GM_FREQ_50Hz                       (0<<0)       // 
#define GM_FREQ_100Hz                      (1<<0)       // 
#define GM_FREQ_200Hz                      (2<<0)       // 
#define GM_FREQ_250Hz                      (3<<0)       // 
#define GM_FREQ_500Hz                      (4<<0)       // 
#define GM_FREQ_1000Hz                     (5<<0)       // 
#define MSK_RANGE_ACC                      (3<<0)       // 
#define GM_ACC_2g                          (0<<0)       // 
#define GM_ACC_4g                          (1<<0)       // 
#define GM_ACC_8g                          (2<<0)       // 
#define GM_ACC_16g                         (3<<0)       // 
#define MSK_RANGE_GYR                      (3<<0)       // 
#define GM_GYR_250dps                      (0<<0)       // 
#define GM_GYR_500dps                      (1<<0)       // 
#define GM_GYR_1000dps                     (2<<0)       // 
#define GM_GYR_2000dps                     (3<<0)       // 
#define B_USE_ACC                          (1<<0)       // 
#define B_USE_GYR                          (1<<1)       // 
#define B_USE_MAG                          (1<<2)       // 
#define MSK_PRIORITIZE                     (3<<0)       // 
#define GM_PRIO_AUTONOMY                   (0<<0)       // Prioritize the autonomy of the device (may have more packet loss)
#define GM_PRIO_AVERAGE                    (1<<0)       // Don't give exclusive prioritization
#define GM_PRIO_DATA                       (2<<0)       // Prioritize the data in order reduze the packet loss (have less autonomy)
#define MSK_IR_LED_PER                     (7<<0)       // 
#define GM_IR_0ms                          (0<<0)       // 
#define GM_IR_20ms                         (1<<0)       // 
#define GM_IR_40ms                         (2<<0)       // 
#define GM_IR_60ms                         (3<<0)       // 
#define GM_IR_80ms                         (4<<0)       // 
#define GM_IR_100ms                        (5<<0)       // 
#define GM_IR_120ms                        (6<<0)       // 
#define GM_IR_140ms                        (7<<0)       // 
#define MSK_TX_POWER                       (3<<0)       // Defines the power used by the device RF front end
#define GM_TX_m18dBm                       (0<<0)       // -18dBm
#define GM_TX_m12dBm                       (1<<0)       // -12dBm
#define GM_TX_m6dBm                        (2<<0)       // -6dBm
#define GM_TX_0dBm                         (3<<0)       // 0dBm
#define MSK_FROM_DI_CONF                   (7<<0)       // Starts the acquisition when write to 1 and stop when write to 0
#define GM_FROM_NOT_USED                   (0<<0)       // 
#define GM_FROM_DI0_RISING                 (1<<0)       // 
#define GM_FROM_DI0_FALLING                (2<<0)       // 
#define GM_FROM_DI1_RISING                 (3<<0)       // 
#define GM_FROM_DI1_FALLING                (4<<0)       // 
#define MSK_DI_CONF                        (15<<0)      // Options for configuration
#define GM_DI_NOT_USED                     (0<<0)       // Digital inputs will not trigger a read
#define GM_DI0_RISING                      (1<<0)       // Rising edge of DI0
#define GM_DI0_FALLING                     (2<<0)       // Falling edge of DI0
#define GM_DI0_RISE_AND_FALL               (3<<0)       // Rising and falling edge of DI0
#define GM_DI1_RISING                      (4<<0)       // Rising edge of DI1
#define GM_DI1_FALLING                     (5<<0)       // Falling edge of DI1
#define GM_DI1_RISE_AND_FALL               (6<<0)       // Rising and falling edge of DI1
#define GM_FREQ_DATA                       (7<<0)       // When a DATA sample arrives
#define GM_FREQ_200                        (8<<0)       // Read at 200 Hz
#define GM_FREQ_250                        (9<<0)       // Read at 250 Hz
#define GM_FREQ_500                        (10<<0)      // Read at 500 Hz
#define GM_FREQ_1000                       (11<<0)      // Read at 1000 Hz
#define MSK_MISC_RUN_CONF                  (3<<0)       // Options for run configuration
#define GM_MISC_RUN_STOP                   (0<<0)       // Stopped
#define GM_MISC_RUN_RUN                    (1<<0)       // Running
#define GM_MISC_RUN_DATA                   (2<<0)       // Run when the DATA is being collected
#define MSK_CAM0_CONF                      (1<<0)       // Options for CAM0
#define GM_CAM0_FREQ                       (0<<0)       // The camera 0 is triggered at a configured frequency
#define GM_CAM0_MOTOR                      (1<<0)       // Used in servo motor controller mode
#define MSK_CAM0_RUN_CONF                  (3<<0)       // Options for run configuration
#define GM_CAM0_RUN_STOP                   (0<<0)       // Stopped
#define GM_CAM0_RUN_RUN                    (1<<0)       // Running
#define GM_CAM0_RUN_DATA                   (2<<0)       // Run when the DATA is being collected
#define MSK_CAM1_CONF                      (1<<0)       // Options for CAM1
#define GM_CAM1_FREQ                       (0<<0)       // The camera 1 is triggered at a configured frequency
#define GM_CAM1_MOTOR                      (1<<0)       // Used in servo motor controller mode
#define MSK_CAM1_RUN_CONF                  (3<<0)       // Options for run configuration
#define GM_CAM1_RUN_STOP                   (0<<0)       // Stopped
#define GM_CAM1_RUN_RUN                    (1<<0)       // Running
#define GM_CAM1_RUN_DATA                   (2<<0)       // Run when the DATA is being collected
#define MSK_DO0_CONF                       (3<<0)       // Options for DO0
#define GM_DO0_DATA                        (0<<0)       // Toggles when DATA register is updated
#define GM_DO0_START                       (1<<0)       // Equal to bit START
#define GM_DO0_REG_DO0                     (2<<0)       // Equal to bit DO0
#define GM_DO0_DATA_SEC                    (3<<0)       // Toggles each second when is acquiring
#define MSK_DO1_CONF                       (3<<0)       // Options for DO1
#define GM_DO1_DATA                        (0<<0)       // Toggles when DATA register is updated
#define GM_DO1_START                       (1<<0)       // Equal to bit START
#define GM_DO1_REG_DO0                     (2<<0)       // Equal to bit DO1
#define GM_DO1_PULSE                       (3<<0)       // Creates a pulse on output DO1 when DO1 bit is written to 1
#define B_EVT0                             (1<<0)       // Events of registers START_DATA, START_STIM and DEV_SELECT
#define B_EVT1                             (1<<1)       // Event of register DATA
#define B_EVT2                             (1<<2)       // Event of register MISC
#define B_EVT3                             (1<<3)       // Event of register CAM0
#define B_EVT4                             (1<<4)       // Event of register CAM1
#define B_EVT5                             (1<<5)       // Event of register DOUT1
#define B_EVT6                             (1<<6)       // Events of registers TEMP, TX_RETRIES, BATTERY and RX_GOOD
#define B_EVT7                             (1<<7)       // Events of registers WEAR_FW_x, WEAR_HW_x, RECEIVER_FW_x, RECEIVER_HW_x

#endif /* _APP_REGS_H_ */