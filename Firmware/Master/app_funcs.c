#include "app_funcs.h"
#include "app_ios_and_regs.h"
#include "hwbp_core.h"
#include "protocol.h"
#include "uart1.h"

#define F_CPU 32000000
#include <util/delay.h>

extern uint16_t pulse_couter_ms;

/************************************************************************/
/* Create pointers to functions                                         */
/************************************************************************/
extern AppRegs app_regs;

void (*app_func_rd_pointer[])(void) = {
	&app_read_REG_START_DATA,
	&app_read_REG_START_STIM,
	&app_read_REG_DATA,
	&app_read_REG_MISC,
	&app_read_REG_CAM0,
	&app_read_REG_CAM1,
	&app_read_REG_DOUT0,
	&app_read_REG_DOUT1,
	&app_read_REG_ACQ_STATUS,
	&app_read_REG_ACQ_EXT_TRIGGER,
	&app_read_REG_DEV_SELECT,
	&app_read_REG_TEMP,
	&app_read_REG_TX_RETRIES,
	&app_read_REG_BATTERY,
	&app_read_REG_BATTERY_RAW,
	&app_read_REG_WEAR_FW_MAJOR,
	&app_read_REG_WEAR_FW_MINOR,
	&app_read_REG_WEAR_HW_MAJOR,
	&app_read_REG_WEAR_HW_MINOR,
	&app_read_REG_RECEIVER_FW_MAJOR,
	&app_read_REG_RECEIVER_FW_MINOR,
	&app_read_REG_RECEIVER_HW_MAJOR,
	&app_read_REG_RECEIVER_HW_MINOR,
	&app_read_REG_RX_GOOD,
	&app_read_REG_RESERVED3,
	&app_read_REG_CONF_DEV_ID,
	&app_read_REG_CONF_SAMPLE_FREQ,
	&app_read_REG_CONF_RANGE_ACC,
	&app_read_REG_CONF_RANGE_GYR,
	&app_read_REG_CONF_ENABLE,
	&app_read_REG_CONF_PRIORITIZATION,
	&app_read_REG_CONF_IR_LEDS_PERIOD,
	&app_read_REG_CONF_TX_POWER,
	&app_read_REG_RESERVED4,
	&app_read_REG_RESERVED5,
	&app_read_REG_STIM_FROM_DI0,
	&app_read_REG_STIM_ON,
	&app_read_REG_STIM_OFF,
	&app_read_REG_STIM_REPS,
	&app_read_REG_STIM_CURRENT,
	&app_read_REG_RESERVED6,
	&app_read_REG_RESERVED7,
	&app_read_REG_MISC_CONF_DI,
	&app_read_REG_MISC_CONF_RUN,
	&app_read_REG_CAM0_CONF,
	&app_read_REG_CAM0_CONF_RUN,
	&app_read_REG_CAM0_FREQ,
	&app_read_REG_CAM0_MMODE_PERIOD,
	&app_read_REG_CAM0_MMODE_PULSE,
	&app_read_REG_CAM1_CONF,
	&app_read_REG_CAM1_CONF_RUN,
	&app_read_REG_CAM1_FREQ,
	&app_read_REG_CAM1_MMODE_PERIOD,
	&app_read_REG_CAM1_MMODE_PULSE,
	&app_read_REG_DO0_CONF,
	&app_read_REG_DO1_CONF,
	&app_read_REG_DO1_PULSE,
	&app_read_REG_RESERVED8,
	&app_read_REG_RESERVED9,
	&app_read_REG_EVNT_ENABLE
};

bool (*app_func_wr_pointer[])(void*) = {
	&app_write_REG_START_DATA,
	&app_write_REG_START_STIM,
	&app_write_REG_DATA,
	&app_write_REG_MISC,
	&app_write_REG_CAM0,
	&app_write_REG_CAM1,
	&app_write_REG_DOUT0,
	&app_write_REG_DOUT1,
	&app_write_REG_ACQ_STATUS,
	&app_write_REG_ACQ_EXT_TRIGGER,
	&app_write_REG_DEV_SELECT,
	&app_write_REG_TEMP,
	&app_write_REG_TX_RETRIES,
	&app_write_REG_BATTERY,
	&app_write_REG_BATTERY_RAW,
	&app_write_REG_WEAR_FW_MAJOR,
	&app_write_REG_WEAR_FW_MINOR,
	&app_write_REG_WEAR_HW_MAJOR,
	&app_write_REG_WEAR_HW_MINOR,
	&app_write_REG_RECEIVER_FW_MAJOR,
	&app_write_REG_RECEIVER_FW_MINOR,
	&app_write_REG_RECEIVER_HW_MAJOR,
	&app_write_REG_RECEIVER_HW_MINOR,
	&app_write_REG_RX_GOOD,
	&app_write_REG_RESERVED3,
	&app_write_REG_CONF_DEV_ID,
	&app_write_REG_CONF_SAMPLE_FREQ,
	&app_write_REG_CONF_RANGE_ACC,
	&app_write_REG_CONF_RANGE_GYR,
	&app_write_REG_CONF_ENABLE,
	&app_write_REG_CONF_PRIORITIZATION,
	&app_write_REG_CONF_IR_LEDS_PERIOD,
	&app_write_REG_CONF_TX_POWER,
	&app_write_REG_RESERVED4,
	&app_write_REG_RESERVED5,
	&app_write_REG_STIM_FROM_DI0,
	&app_write_REG_STIM_ON,
	&app_write_REG_STIM_OFF,
	&app_write_REG_STIM_REPS,
	&app_write_REG_STIM_CURRENT,
	&app_write_REG_RESERVED6,
	&app_write_REG_RESERVED7,
	&app_write_REG_MISC_CONF_DI,
	&app_write_REG_MISC_CONF_RUN,
	&app_write_REG_CAM0_CONF,
	&app_write_REG_CAM0_CONF_RUN,
	&app_write_REG_CAM0_FREQ,
	&app_write_REG_CAM0_MMODE_PERIOD,
	&app_write_REG_CAM0_MMODE_PULSE,
	&app_write_REG_CAM1_CONF,
	&app_write_REG_CAM1_CONF_RUN,
	&app_write_REG_CAM1_FREQ,
	&app_write_REG_CAM1_MMODE_PERIOD,
	&app_write_REG_CAM1_MMODE_PULSE,
	&app_write_REG_DO0_CONF,
	&app_write_REG_DO1_CONF,
	&app_write_REG_DO1_PULSE,
	&app_write_REG_RESERVED8,
	&app_write_REG_RESERVED9,
	&app_write_REG_EVNT_ENABLE
};


/************************************************************************/
/* REG_START_DATA                                                       */
/************************************************************************/
void app_read_REG_START_DATA(void) {}

extern uint8_t wake_up_counter;
extern uint16_t wake_up_timeout;
extern wake_up_t disable_rx;
extern void fill_wake_up(void);

#define STATE_STANDBY 0
#define STATE_COMMANDRECEIVED 1
#define STATE_RESETDONE 2
#define STATE_WAITINGFORFIRSTSAMPLE 3
#define STATE_ACQUIRING 4
extern uint8_t StartState;

extern uint8_t stop_timeout_s;
extern bool first_tx_retries;
extern uint8_t first_packets;

bool stop_camera0 = false;
bool stop_camera1 = false;

bool app_write_REG_START_DATA(void *a)
{
	if (*((uint8_t*)a) & ~B_START)
		return false;

	if (*((uint8_t*)a) == B_START)
	{
		if (StartState != STATE_STANDBY)
		{
			return false;
		}
		else
		{
			if (app_regs.REG_DEV_SELECT == GM_SEL_RF1)
			{
				//clr_RST_RF1;
				clr_EN_RF1;
                first_packets = 5;      // Number of first packets to ignore
			}
		
			/* Start data acquisition */
			StartState = STATE_COMMANDRECEIVED;
			wake_up_counter = 100;
			wake_up_timeout = 4000; // 4 seconds
            stop_timeout_s = 0;     // Not used yet
            
            first_tx_retries = true;

			app_regs.REG_START_DATA = B_START;

			return true;
		}
	}
	else
	{
		if (StartState == STATE_ACQUIRING)
		{
			/* Stop data acquisition */
			uart1_xmit((uint8_t*)(&disable_rx), LEN_DISABLE_RX);

			if (app_regs.REG_CAM0_CONF == GM_CAM0_FREQ && app_regs.REG_CAM0_CONF_RUN == GM_CAM0_RUN_DATA)
			{
				stop_camera0 = true;
			}

			if (app_regs.REG_CAM1_CONF == GM_CAM1_FREQ && app_regs.REG_CAM1_CONF_RUN == GM_CAM1_RUN_DATA)
			{
				stop_camera1 = true;
			}

			if (app_regs.REG_DO0_CONF == GM_DO0_START)
			{
				app_regs.REG_DOUT0 = 0;
				app_write_REG_DOUT0(&app_regs.REG_DOUT0);
			}

			if (app_regs.REG_DO1_CONF == GM_DO1_START)
			{
				app_regs.REG_DOUT1 = 0;
				app_write_REG_DOUT1(&app_regs.REG_DOUT1);
				pulse_couter_ms = 0;
			}
				
			/* Start or stop inputs if configured that way */
			app_write_REG_MISC_CONF_DI(&app_regs.REG_MISC_CONF_DI);
			
			app_regs.REG_ACQ_STATUS = 0;

			if (app_regs.REG_EVNT_ENABLE & B_EVT0)
			{
				core_func_send_event(ADD_REG_ACQ_STATUS, true);
			}

			StartState = STATE_STANDBY;
			app_regs.REG_START_DATA = 0;
			return true;
		}
		else if (StartState == STATE_STANDBY)
		{
			app_regs.REG_START_DATA = 0;
			return true;
		}
		else
		{
			return false;
		}
	}
}


/************************************************************************/
/* REG_START_STIM                                                       */
/************************************************************************/
extern bool fill_stim (void);
//extern bool send_stim_command;

extern stim1_t stim_type1;
extern stim2_t stim_type2;
extern stim3_t stim_type3;

bool stim_was_sent = false;

extern uint8_t send_stim_cmd_type2_counter;

void app_read_REG_START_STIM(void)
{
    if (stim_was_sent)
    {
        app_regs.REG_START_STIM = B_STIM;
    }
    else
    {
        app_regs.REG_START_STIM = 0;
    }
}
bool app_write_REG_START_STIM(void *a)
{
    if (*((uint8_t*)a) == B_STIM)
    {
        if (app_regs.REG_DEV_SELECT == GM_SEL_WIRED)
        {
            if (fill_stim())
            {
                uart1_xmit((uint8_t*)(&stim_type3), LEN_STIM_3);
                stim_was_sent = true;
            }
        }
        
        /* Send stimulation command only if the RF1 device is acquiring */
        if ((app_regs.REG_DEV_SELECT == GM_SEL_RF1) && (StartState == STATE_ACQUIRING))
        {
            if (fill_stim())
            {
                if (1)
                {
                    uart1_xmit((uint8_t*)(&stim_type2), LEN_STIM_2);
                    uint8_t dummy[7] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};
                    uart1_xmit(dummy, 7);
                    stim_was_sent = true;
                }
                else
                {
                    send_stim_cmd_type2_counter = 0;
                    uart1_xmit((uint8_t*)(&stim_type2), 1);
                        
                    timer_type0_enable(&TCE0, TIMER_PRESCALER_DIV256, 32, INT_LEVEL_LOW);   // 256 us
                }
            }
        }
    }        
    
    app_regs.REG_START_STIM = *((uint8_t*)a);
    
	return true;
}


/************************************************************************/
/* REG_DATA                                                             */
/************************************************************************/
// This register is an array with 9 positions
void app_read_REG_DATA(void) {}
bool app_write_REG_DATA(void *a) { return false; }


/************************************************************************/
/* REG_MISC                                                             */
/************************************************************************/
extern uint16_t AdcOffset;

void app_read_REG_MISC(void)
{
	app_regs.REG_MISC = read_DI0 ? 0x4000 : 0;
	app_regs.REG_MISC |= read_DI1 ? 0x8000 : 0;
	
	ADCA_CH0_CTRL |= ADC_CH_START_bm;						// Force the first conversion
	while(!(ADCA_CH0_INTFLAGS & ADC_CH_CHIF_bm));		// Wait for conversion to finish
	ADCA_CH0_INTFLAGS = ADC_CH_CHIF_bm;						// Clear interrupt bit

	if (ADCA_CH0_RES > AdcOffset)
		app_regs.REG_MISC |= (ADCA_CH0_RES & 0x0FFF) - AdcOffset;		
}

bool app_write_REG_MISC(void *a) { return false; }


/************************************************************************/
/* REG_CAM0                                                             */
/************************************************************************/
void app_read_REG_CAM0(void) {}
bool app_write_REG_CAM0(void *a)
{
	if (*((uint8_t*)a) & ~B_CAM0)
		return false;

	if (*((uint8_t*)a))
	{
		set_CAM0;
		app_regs.REG_CAM0 = B_CAM0;
	}
	else
	{
		clr_CAM0;
		app_regs.REG_CAM0 = 0;
	}

	return true;
}


/************************************************************************/
/* REG_CAM1                                                             */
/************************************************************************/
void app_read_REG_CAM1(void) {}
bool app_write_REG_CAM1(void *a)
{
	if (*((uint8_t*)a) & ~B_CAM1)
		return false;

	if (*((uint8_t*)a))
	{
		set_CAM1;
		app_regs.REG_CAM1 = B_CAM1;
	}
	else
	{
		clr_CAM1;
		app_regs.REG_CAM1 = 0;
	}

	return true;
}


/************************************************************************/
/* REG_DOUT0                                                            */
/************************************************************************/
void app_read_REG_DOUT0(void) {}
bool app_write_REG_DOUT0(void *a)
{
	uint8_t reg = *((uint8_t*)a);

	if (reg & ~B_DO0)
		return false;

	if (reg)
	{
		set_DO0;
		app_regs.REG_DOUT0 = B_DO0;
	}
	else
	{
		clr_DO0;
		app_regs.REG_DOUT0 = 0;
	}

	return true;
}


/************************************************************************/
/* REG_DOUT1                                                            */
/************************************************************************/
void app_read_REG_DOUT1(void) {}
bool app_write_REG_DOUT1(void *a)
{
	uint8_t reg = *((uint8_t*)a);

	if (reg & ~B_DO1)
		return false;

	if (reg)
	{
		set_DO1;
		app_regs.REG_DOUT1 = B_DO1;

		if (app_regs.REG_DO1_CONF == GM_DO1_PULSE)
		{
			pulse_couter_ms = app_regs.REG_DO1_PULSE + 1;
		}
	}
	else
	{
		clr_DO1;
		app_regs.REG_DOUT1 = 0;
	}

	return true;
}


/************************************************************************/
/* REG_ACQ_STATUS                                                        */
/************************************************************************/
void app_read_REG_ACQ_STATUS(void) {}
bool app_write_REG_ACQ_STATUS(void *a) { return true; }


/************************************************************************/
/* REG_ACQ_EXT_TRIGGER                                                  */
/************************************************************************/
void app_read_REG_ACQ_EXT_TRIGGER(void) {}
bool app_write_REG_ACQ_EXT_TRIGGER(void *a)
{
	if (*((uint8_t*)a) & ~MSK_EXT_CONF)
		return false;
	
	app_regs.REG_ACQ_EXT_TRIGGER = *((uint8_t*)a);
	
	return true; 
}


/************************************************************************/
/* REG_DEV_SELECT                                                       */
/************************************************************************/
void app_read_REG_DEV_SELECT(void) {}
bool app_write_REG_DEV_SELECT(void *a)
{
	if (*((uint8_t*)a) > GM_SEL_RF2)
		return false;

	if (StartState != STATE_STANDBY)
		return false;

	app_regs.REG_DEV_SELECT = *((uint8_t*)a);

	clr_UART_RF1;
	clr_UART_RF2;
	clr_UART_WIRED;
	clr_EN_RF1;
	clr_EN_RF2;
	clr_EN_WIRED;

	if(app_regs.REG_DEV_SELECT == GM_SEL_RF1) { set_EN_RF1; set_UART_RF1; }
	if(app_regs.REG_DEV_SELECT == GM_SEL_RF2) { set_EN_RF2; set_UART_RF2; }
	if(app_regs.REG_DEV_SELECT == GM_SEL_WIRED) { set_EN_WIRED; set_UART_WIRED; }

	return true;
}


/************************************************************************/
/* REG_TEMP                                                             */
/************************************************************************/
void app_read_REG_TEMP(void) {}
bool app_write_REG_TEMP(void *a) { return false; }


/************************************************************************/
/* REG_TX_RETRIES                                                       */
/************************************************************************/
void app_read_REG_TX_RETRIES(void) {}
bool app_write_REG_TX_RETRIES(void *a) { return false; }


/************************************************************************/
/* REG_BATTERY                                                          */
/************************************************************************/
void app_read_REG_BATTERY(void) {}
bool app_write_REG_BATTERY(void *a) { return false; }


/************************************************************************/
/* REG_BATTERY_RAW_MAX                                                  */
/************************************************************************/
void app_read_REG_BATTERY_RAW(void) {}
bool app_write_REG_BATTERY_RAW(void *a) { return false; }


/************************************************************************/
/* REG_WEAR_FW_MAJOR                                                    */
/************************************************************************/
void app_read_REG_WEAR_FW_MAJOR(void) {}
bool app_write_REG_WEAR_FW_MAJOR(void *a) { return false; }


/************************************************************************/
/* REG_WEAR_FW_MINOR                                                    */
/************************************************************************/
void app_read_REG_WEAR_FW_MINOR(void) {}
bool app_write_REG_WEAR_FW_MINOR(void *a) { return false; }


/************************************************************************/
/* REG_WEAR_HW_MAJOR                                                    */
/************************************************************************/
void app_read_REG_WEAR_HW_MAJOR(void) {}
bool app_write_REG_WEAR_HW_MAJOR(void *a) { return false; }


/************************************************************************/
/* REG_WEAR_HW_MINOR                                                    */
/************************************************************************/
void app_read_REG_WEAR_HW_MINOR(void) {}
bool app_write_REG_WEAR_HW_MINOR(void *a) { return false; }


/************************************************************************/
/* REG_RECEIVER_FW_MAJOR                                                */
/************************************************************************/
void app_read_REG_RECEIVER_FW_MAJOR(void) {}
bool app_write_REG_RECEIVER_FW_MAJOR(void *a) { return false; }


/************************************************************************/
/* REG_RECEIVER_FW_MINOR                                                */
/************************************************************************/
void app_read_REG_RECEIVER_FW_MINOR(void) {}
bool app_write_REG_RECEIVER_FW_MINOR(void *a) { return false; }


/************************************************************************/
/* REG_RECEIVER_HW_MAJOR                                                */
/************************************************************************/
void app_read_REG_RECEIVER_HW_MAJOR(void) {}
bool app_write_REG_RECEIVER_HW_MAJOR(void *a) { return false; }


/************************************************************************/
/* REG_RECEIVER_HW_MINOR                                                */
/************************************************************************/
void app_read_REG_RECEIVER_HW_MINOR(void) {}
bool app_write_REG_RECEIVER_HW_MINOR(void *a) { return false; }


/************************************************************************/
/* REG_RX_GOOD                                                          */
/************************************************************************/
void app_read_REG_RX_GOOD(void) {}
bool app_write_REG_RX_GOOD(void *a) { return false; }


/************************************************************************/
/* REG_RESERVED3                                                        */
/************************************************************************/
void app_read_REG_RESERVED3(void) {}
bool app_write_REG_RESERVED3(void *a) { return true; }


/************************************************************************/
/* REG_CONF_DEV_ID                                                      */
/************************************************************************/
void app_read_REG_CONF_DEV_ID(void) {}
bool app_write_REG_CONF_DEV_ID(void *a)
{
	if (*((uint16_t*)a) > 8191)
		return false;

	app_regs.REG_CONF_DEV_ID = *((uint16_t*)a);
	return true;
}


/************************************************************************/
/* REG_CONF_SAMPLE_FREQ                                                 */
/************************************************************************/
void app_read_REG_CONF_SAMPLE_FREQ(void) {}
bool app_write_REG_CONF_SAMPLE_FREQ(void *a)
{
	if (*((uint8_t*)a) & ~ MSK_SENSOR_FREQ)
		return false;

	if (*((uint8_t*)a) == GM_FREQ_1000Hz)
	{
		if ((app_regs.REG_CONF_ENABLE == B_USE_ACC) || (app_regs.REG_CONF_ENABLE == B_USE_GYR))
		{
			app_regs.REG_CONF_SAMPLE_FREQ = *((uint8_t*)a);
			return true;
		}
		else
		{
			return false;
		}
	}

	app_regs.REG_CONF_SAMPLE_FREQ = *((uint8_t*)a);
	return true;
}


/************************************************************************/
/* REG_CONF_RANGE_ACC                                                   */
/************************************************************************/
void app_read_REG_CONF_RANGE_ACC(void) {}
bool app_write_REG_CONF_RANGE_ACC(void *a)
{
	if (*((uint8_t*)a) & ~MSK_RANGE_ACC)
		return false;

	app_regs.REG_CONF_RANGE_ACC = *((uint8_t*)a);
	return true;
}


/************************************************************************/
/* REG_CONF_RANGE_GYR                                                   */
/************************************************************************/
void app_read_REG_CONF_RANGE_GYR(void) {}
bool app_write_REG_CONF_RANGE_GYR(void *a)
{
	if (*((uint8_t*)a) & ~MSK_RANGE_GYR)
		return false;

	app_regs.REG_CONF_RANGE_GYR = *((uint8_t*)a);
	return true;
}


/************************************************************************/
/* REG_CONF_ENABLE                                                      */
/************************************************************************/
void app_read_REG_CONF_ENABLE(void) {}
bool app_write_REG_CONF_ENABLE(void *a)
{
	if ((*((uint8_t*)a) & ~7) || (*((uint8_t*)a) == 0) || (*((uint8_t*)a) == B_USE_MAG))
		return false;

	if (app_regs.REG_CONF_SAMPLE_FREQ == GM_FREQ_1000Hz)
	{
		if ((*((uint8_t*)a) == B_USE_ACC) || (*((uint8_t*)a) == B_USE_GYR))
		{
			app_regs.REG_CONF_ENABLE = *((uint8_t*)a);
			return true;
		}
		else
		{
			return false;
		}
	}

	app_regs.REG_CONF_ENABLE = *((uint8_t*)a);

	return true;
}


/************************************************************************/
/* REG_CONF_PRIORITIZATION                                              */
/************************************************************************/
void app_read_REG_CONF_PRIORITIZATION(void) {}
bool app_write_REG_CONF_PRIORITIZATION(void *a)
{
	if (*((uint8_t*)a) & ~MSK_PRIORITIZE)
		return false;

	app_regs.REG_CONF_PRIORITIZATION = *((uint8_t*)a);
	return true;
}


/************************************************************************/
/* REG_CONF_IR_LEDS_PERIOD                                              */
/************************************************************************/
void app_read_REG_CONF_IR_LEDS_PERIOD(void) {}

bool app_write_REG_CONF_IR_LEDS_PERIOD(void *a)
{
	if (*((uint8_t*)a) & ~MSK_IR_LED_PER)
		return false;

	app_regs.REG_CONF_IR_LEDS_PERIOD = *((uint8_t*)a);
	return true;
}


/************************************************************************/
/* REG_CONF_TX_POWER                                                    */
/************************************************************************/
void app_read_REG_CONF_TX_POWER(void) {}
bool app_write_REG_CONF_TX_POWER(void *a)
{
	if (*((uint8_t*)a) & ~MSK_TX_POWER)
		return false;

	app_regs.REG_CONF_TX_POWER = *((uint8_t*)a);
	return true;
}


/************************************************************************/
/* REG_RESERVED4                                                        */
/************************************************************************/
void app_read_REG_RESERVED4(void) {}
bool app_write_REG_RESERVED4(void *a) { return true; }


/************************************************************************/
/* REG_RESERVED5                                                        */
/************************************************************************/
void app_read_REG_RESERVED5(void) {}
bool app_write_REG_RESERVED5(void *a) { return true; }


/************************************************************************/
/* REG_STIM_FROM_DI0                                                    */
/************************************************************************/
void app_read_REG_STIM_FROM_DI0(void) {}
bool app_write_REG_STIM_FROM_DI0(void *a)
{
	if (*((uint8_t*)a) & ~MSK_FROM_DI_CONF)
		return false;

	app_regs.REG_STIM_FROM_DI0 = *((uint8_t*)a);
	return true;
}


/************************************************************************/
/* REG_STIM_ON                                                          */
/************************************************************************/
void app_read_REG_STIM_ON(void) {}
bool app_write_REG_STIM_ON(void *a)
{
	if (*((uint16_t*)a) == 0 || *((uint16_t*)a) > 60000)
		return false;

	app_regs.REG_STIM_ON = *((uint16_t*)a);
	return true;
}


/************************************************************************/
/* REG_STIM_OFF                                                         */
/************************************************************************/
void app_read_REG_STIM_OFF(void) {}
bool app_write_REG_STIM_OFF(void *a)
{
	if (*((uint16_t*)a) == 0 || *((uint16_t*)a) > 60000)
		return false;

	app_regs.REG_STIM_OFF = *((uint16_t*)a);
	return true;
}


/************************************************************************/
/* REG_STIM_REPS                                                        */
/************************************************************************/
void app_read_REG_STIM_REPS(void) {}
bool app_write_REG_STIM_REPS(void *a)
{
	if (*((uint16_t*)a) == 0 || *((uint16_t*)a) > 60000)
		return false;

	app_regs.REG_STIM_REPS = *((uint16_t*)a);
	return true;
}


/************************************************************************/
/* REG_STIM_CURRENT                                                     */
/************************************************************************/
void app_read_REG_STIM_CURRENT(void) {}
bool app_write_REG_STIM_CURRENT(void *a)
{
    app_regs.REG_STIM_CURRENT = *((uint16_t*)a);
	return true;
}


/************************************************************************/
/* REG_RESERVED6                                                        */
/************************************************************************/
void app_read_REG_RESERVED6(void) {}
bool app_write_REG_RESERVED6(void *a) { return true; }


/************************************************************************/
/* REG_RESERVED7                                                        */
/************************************************************************/
void app_read_REG_RESERVED7(void) {}
bool app_write_REG_RESERVED7(void *a) { return true; }


/************************************************************************/
/* REG_MISC_CONF_DI                                                     */
/************************************************************************/
void app_read_REG_MISC_CONF_DI(void) {}
bool app_write_REG_MISC_CONF_DI(void *a)
{
	uint8_t reg = *((uint8_t*)a);

	if (reg & ~MSK_DI_CONF)
		return false;

	switch (reg)
	{
		case GM_DI_NOT_USED:
		case GM_DI0_RISING:
		case GM_DI0_FALLING:
		case GM_DI0_RISE_AND_FALL:
		case GM_DI1_RISING:
		case GM_DI1_FALLING:
		case GM_DI1_RISE_AND_FALL:
		case GM_FREQ_DATA:
			timer_type1_stop(&TCD1);
			break;
	}

	if (app_regs.REG_MISC_CONF_RUN == GM_MISC_RUN_RUN || ((app_regs.REG_MISC_CONF_RUN == GM_MISC_RUN_DATA) && (app_regs.REG_ACQ_STATUS == B_STATUS)))
	{
		switch(reg)
		{
			case GM_FREQ_200:
				if (TCD1_CTRLA == TC_CLKSEL_OFF_gc)
					timer_type1_enable(&TCD1, TIMER_PRESCALER_DIV256, 625, INT_LEVEL_LOW);
				break;
			
			case GM_FREQ_250:
				if (TCD1_CTRLA == TC_CLKSEL_OFF_gc)
					timer_type1_enable(&TCD1, TIMER_PRESCALER_DIV256, 500, INT_LEVEL_LOW);
				break;
				
			case GM_FREQ_500:
				if (TCD1_CTRLA == TC_CLKSEL_OFF_gc)
					timer_type1_enable(&TCD1, TIMER_PRESCALER_DIV256, 250, INT_LEVEL_LOW);
				break;
				
			case GM_FREQ_1000:			
				if (TCD1_CTRLA == TC_CLKSEL_OFF_gc)
					timer_type1_enable(&TCD1, TIMER_PRESCALER_DIV256, 125, INT_LEVEL_LOW);
				break;
		}
	}
	else
	{
		timer_type1_stop(&TCD1);
	}

	app_regs.REG_MISC_CONF_DI = reg;
	return true;
}


/************************************************************************/
/* REG_MISC_CONF_RUN                                                    */
/************************************************************************/
void app_read_REG_MISC_CONF_RUN(void) {}
bool app_write_REG_MISC_CONF_RUN(void *a)
{
	if (*((uint8_t*)a) & ~MSK_MISC_RUN_CONF)
		return false;

	app_regs.REG_MISC_CONF_RUN = *((uint8_t*)a);

	/* Start or stop inputs if configured that way */
	app_write_REG_MISC_CONF_DI(&app_regs.REG_MISC_CONF_DI);

	return true;
}


/************************************************************************/
/* REG_CAM0_CONF                                                        */
/************************************************************************/
void app_read_REG_CAM0_CONF(void) {}

void start_cam0 (void)
{
	if (app_regs.REG_CAM0_FREQ < 8)
	{
		timer_type0_pwm(&TCD0, TIMER_PRESCALER_DIV256, (32000000/256)/app_regs.REG_CAM0_FREQ, (16000000/256)/app_regs.REG_CAM0_FREQ, INT_LEVEL_LOW, INT_LEVEL_LOW);
	}
	else if (app_regs.REG_CAM0_FREQ < 64)
	{
		timer_type0_pwm(&TCD0, TIMER_PRESCALER_DIV64, (32000000/64)/app_regs.REG_CAM0_FREQ, (16000000/64)/app_regs.REG_CAM0_FREQ, INT_LEVEL_LOW, INT_LEVEL_LOW);
	}
	else if (app_regs.REG_CAM0_FREQ < 128)
	{
		timer_type0_pwm(&TCD0, TIMER_PRESCALER_DIV8, (32000000/8)/app_regs.REG_CAM0_FREQ, (16000000/8)/app_regs.REG_CAM0_FREQ, INT_LEVEL_LOW, INT_LEVEL_LOW);
	}	
	else if (app_regs.REG_CAM0_FREQ < 256)
	{
		timer_type0_pwm(&TCD0, TIMER_PRESCALER_DIV4, (32000000/4)/app_regs.REG_CAM0_FREQ, (16000000/4)/app_regs.REG_CAM0_FREQ, INT_LEVEL_LOW, INT_LEVEL_LOW);
	}
	else if (app_regs.REG_CAM0_FREQ <= 600)
	{
		timer_type0_pwm(&TCD0, TIMER_PRESCALER_DIV2, (32000000/2)/app_regs.REG_CAM0_FREQ, (16000000/2)/app_regs.REG_CAM0_FREQ, INT_LEVEL_LOW, INT_LEVEL_LOW);
	}
}

bool app_write_REG_CAM0_CONF(void *a)
{
	if (*((uint8_t*)a) & ~MSK_CAM0_CONF)
		return false;	

	if (*((uint8_t*)a) == GM_CAM0_MOTOR)
	{
		app_regs.REG_CAM0 = 0;

		if (*((uint8_t*)a) != app_regs.REG_CAM0_CONF)
		{
			/* If register CAM0_CONF changed, start motor control */
			timer_type0_pwm(&TCD0, TIMER_PRESCALER_DIV64, (app_regs.REG_CAM0_MMODE_PERIOD >> 1), (app_regs.REG_CAM0_MMODE_PULSE >> 1), INT_LEVEL_OFF, INT_LEVEL_OFF);
		}
	}
	else
	{      
      if (app_regs.REG_CAM0_CONF == GM_CAM0_MOTOR)
      {
         // If the previous configuration was the motor, stop it
         timer_type0_stop(&TCD0);
         clr_CAM0;
      }
      
		if (app_regs.REG_CAM0_CONF_RUN == GM_CAM0_RUN_STOP)
		{
			stop_camera0 = true;
		}
		else if (app_regs.REG_CAM0_CONF_RUN == GM_CAM0_RUN_RUN)
		{
    		clr_CAM0;
    		_delay_us(16);      // Measured, gives around 55us before the first trigger pulse
			start_cam0();
            stop_camera0 = false;
		}
		else if (app_regs.REG_CAM0_CONF_RUN == GM_CAM0_RUN_DATA)
		{
			if (app_regs.REG_ACQ_STATUS & B_STATUS)
			{    
                clr_CAM0;
    			_delay_us(16);      // Measured, gives around 55us before the first trigger pulse
				start_cam0();
                stop_camera0 = false;				
			}
			else
			{
				stop_camera0 = true;
			}
		}
	}

	app_regs.REG_CAM0_CONF = *((uint8_t*)a);
	return true;
}


/************************************************************************/
/* REG_CAM0_CONF_RUN                                                    */
/************************************************************************/
void app_read_REG_CAM0_CONF_RUN(void) {}
bool app_write_REG_CAM0_CONF_RUN(void *a)
{
	if (*((uint8_t*)a) & ~MSK_CAM0_RUN_CONF)
		return false;

	if (app_regs.REG_CAM0_CONF_RUN != *((uint8_t*)a))
	{
		app_regs.REG_CAM0_CONF_RUN = *((uint8_t*)a);
		app_write_REG_CAM0_CONF(&app_regs.REG_CAM0_CONF);
	}
	
	return true;
}


/************************************************************************/
/* REG_CAM0_FREQ                                                        */
/************************************************************************/
void app_read_REG_CAM0_FREQ(void) {}
bool app_write_REG_CAM0_FREQ(void *a)
{
	if (*((uint16_t*)a) < 2 || *((uint16_t*)a) > 600)
		return false;
	
	if (app_regs.REG_CAM0_FREQ != *((uint8_t*)a))
	{
		app_regs.REG_CAM0_FREQ = *((uint16_t*)a);
		app_write_REG_CAM0_CONF(&app_regs.REG_CAM0_CONF);
	}

	return true;
}


/************************************************************************/
/* REG_CAM0_MMODE_PERIOD                                                */
/************************************************************************/
void app_read_REG_CAM0_MMODE_PERIOD(void) {}
bool app_write_REG_CAM0_MMODE_PERIOD(void *a)
{
	if (*((uint16_t*)a) < 1)
		return false;
	
	app_regs.REG_CAM0_MMODE_PERIOD = *((uint16_t*)a);

	if (app_regs.REG_CAM0_CONF == GM_CAM0_MOTOR)
	{
		TCD0_PER = (app_regs.REG_CAM0_MMODE_PERIOD >> 1) - 1;
	}
	
	return true;
}


/************************************************************************/
/* REG_CAM0_MMODE_PULSE                                                 */
/************************************************************************/
void app_read_REG_CAM0_MMODE_PULSE(void) {}
bool app_write_REG_CAM0_MMODE_PULSE(void *a)
{
	if (*((uint16_t*)a) < 1)
	return false;

	app_regs.REG_CAM0_MMODE_PULSE = *((uint16_t*)a);

	if (app_regs.REG_CAM0_CONF == GM_CAM0_MOTOR)
	{
		TCD0_CCA = (app_regs.REG_CAM0_MMODE_PULSE  >> 1) - 1;
	}

	return true;
}

/************************************************************************/
/* REG_CAM1_CONF                                                        */
/************************************************************************/
void app_read_REG_CAM1_CONF(void) {}

void start_cam1 (void)
{
	if (app_regs.REG_CAM1_FREQ < 8)
	{
		timer_type0_pwm(&TCC0, TIMER_PRESCALER_DIV256, (32000000/256)/app_regs.REG_CAM1_FREQ, (16000000/256)/app_regs.REG_CAM1_FREQ, INT_LEVEL_LOW, INT_LEVEL_LOW);
	}
	else if (app_regs.REG_CAM1_FREQ < 64)
	{
		timer_type0_pwm(&TCC0, TIMER_PRESCALER_DIV64, (32000000/64)/app_regs.REG_CAM1_FREQ, (16000000/64)/app_regs.REG_CAM1_FREQ, INT_LEVEL_LOW, INT_LEVEL_LOW);
	}
	else if (app_regs.REG_CAM1_FREQ < 128)
	{
		timer_type0_pwm(&TCC0, TIMER_PRESCALER_DIV8, (32000000/8)/app_regs.REG_CAM1_FREQ, (16000000/8)/app_regs.REG_CAM1_FREQ, INT_LEVEL_LOW, INT_LEVEL_LOW);
	}
	else if (app_regs.REG_CAM1_FREQ < 256)
	{
		timer_type0_pwm(&TCC0, TIMER_PRESCALER_DIV4, (32000000/4)/app_regs.REG_CAM1_FREQ, (16000000/4)/app_regs.REG_CAM1_FREQ, INT_LEVEL_LOW, INT_LEVEL_LOW);
	}
	else if (app_regs.REG_CAM1_FREQ <= 600)
	{
		timer_type0_pwm(&TCC0, TIMER_PRESCALER_DIV2, (32000000/2)/app_regs.REG_CAM1_FREQ, (16000000/2)/app_regs.REG_CAM1_FREQ, INT_LEVEL_LOW, INT_LEVEL_LOW);
	}
}

bool app_write_REG_CAM1_CONF(void *a)
{
	if (*((uint8_t*)a) & ~MSK_CAM1_CONF)
		return false;

	if (*((uint8_t*)a) == GM_CAM1_MOTOR)
	{
		app_regs.REG_CAM1 = 0;

		if (*((uint8_t*)a) != app_regs.REG_CAM1_CONF)
		{
			/* If register CAM1_CONF changed, start motor control */
			timer_type0_pwm(&TCC0, TIMER_PRESCALER_DIV64, (app_regs.REG_CAM1_MMODE_PERIOD >> 1), (app_regs.REG_CAM1_MMODE_PULSE >> 1), INT_LEVEL_OFF, INT_LEVEL_OFF);
		}
	}
	else
	{
      if (app_regs.REG_CAM1_CONF == GM_CAM1_MOTOR)
      {
         // If the previous configuration was the motor, stop it
         timer_type0_stop(&TCC0);
         clr_CAM1;
      }
      
		if (app_regs.REG_CAM1_CONF_RUN == GM_CAM1_RUN_STOP)
		{
			stop_camera1 = true;
		}
		else if (app_regs.REG_CAM1_CONF_RUN == GM_CAM1_RUN_RUN)
		{
            clr_CAM1;
            _delay_us(16);      // Measured, gives around 55us before the first trigger pulse
			start_cam1();
            stop_camera1 = false;
		}
		else if (app_regs.REG_CAM1_CONF_RUN == GM_CAM1_RUN_DATA)
		{
			if (app_regs.REG_ACQ_STATUS & B_STATUS)
			{
                clr_CAM1;
                _delay_us(16);      // Measured, gives around 55us before the first trigger pulse
				start_cam1();
                stop_camera1 = false;
			}
			else
			{
				stop_camera1 = true;
			}
		}
	}

	app_regs.REG_CAM1_CONF = *((uint8_t*)a);
	return true;
}


/************************************************************************/
/* REG_CAM1_CONF_RUN                                                    */
/************************************************************************/
void app_read_REG_CAM1_CONF_RUN(void) {}
bool app_write_REG_CAM1_CONF_RUN(void *a)
{
	if (*((uint8_t*)a) & ~MSK_CAM1_RUN_CONF)
	return false;

	if (app_regs.REG_CAM1_CONF_RUN != *((uint8_t*)a))
	{
		app_regs.REG_CAM1_CONF_RUN = *((uint8_t*)a);
		app_write_REG_CAM1_CONF(&app_regs.REG_CAM1_CONF);
	}
	
	return true;
}


/************************************************************************/
/* REG_CAM1_FREQ                                                        */
/************************************************************************/
void app_read_REG_CAM1_FREQ(void) {}
bool app_write_REG_CAM1_FREQ(void *a)
{
	if (*((uint16_t*)a) < 2 || *((uint16_t*)a) > 600)
	return false;
	
	if (app_regs.REG_CAM1_FREQ != *((uint8_t*)a))
	{
		app_regs.REG_CAM1_FREQ = *((uint16_t*)a);
		app_write_REG_CAM1_CONF(&app_regs.REG_CAM1_CONF);
	}

	return true;
}


/************************************************************************/
/* REG_CAM1_MMODE_PERIOD                                                */
/************************************************************************/
void app_read_REG_CAM1_MMODE_PERIOD(void) {}
bool app_write_REG_CAM1_MMODE_PERIOD(void *a)
{
	if (*((uint16_t*)a) < 1)
	return false;
	
	app_regs.REG_CAM1_MMODE_PERIOD = *((uint16_t*)a);

	if (app_regs.REG_CAM1_CONF == GM_CAM1_MOTOR)
	{
		TCC0_PER = (app_regs.REG_CAM1_MMODE_PERIOD >> 1) - 1;
	}
	
	return true;
}


/************************************************************************/
/* REG_CAM1_MMODE_PULSE                                                 */
/************************************************************************/
void app_read_REG_CAM1_MMODE_PULSE(void) {}
bool app_write_REG_CAM1_MMODE_PULSE(void *a)
{
	if (*((uint16_t*)a) < 1)
	return false;

	app_regs.REG_CAM1_MMODE_PULSE = *((uint16_t*)a);

	if (app_regs.REG_CAM1_CONF == GM_CAM1_MOTOR)
	{
		TCC0_CCA = (app_regs.REG_CAM1_MMODE_PULSE  >> 1) - 1;
	}

	return true;
}


/************************************************************************/
/* REG_DO0_CONF                                                         */
/************************************************************************/
void app_read_REG_DO0_CONF(void) {}
bool app_write_REG_DO0_CONF(void *a)
{
	uint8_t reg = *((uint8_t*)a);

	if (reg & ~MSK_DO0_CONF)
		return false;

	if (reg == GM_DO0_START)
	{
		if (app_regs.REG_ACQ_STATUS & B_STATUS)
		{
			app_regs.REG_DOUT0 = 1;
			app_write_REG_DOUT0(&app_regs.REG_DOUT0);
		}
		else
		{
			app_regs.REG_DOUT0 = 0;
			app_write_REG_DOUT0(&app_regs.REG_DOUT0);
		}
	}

	app_regs.REG_DO0_CONF = reg;
	return true;
}


/************************************************************************/
/* REG_DO1_CONF                                                         */
/************************************************************************/
void app_read_REG_DO1_CONF(void) {}
bool app_write_REG_DO1_CONF(void *a)
{
	uint8_t reg = *((uint8_t*)a);
	
	if (reg & ~MSK_DO1_CONF)
		return false;

	if (reg == GM_DO1_START)
	{
		if (app_regs.REG_ACQ_STATUS & B_STATUS)
		{
			app_regs.REG_DOUT1 = 1;
			app_write_REG_DOUT1(&app_regs.REG_DOUT1);
			pulse_couter_ms = 0;
		}
		else
		{
			app_regs.REG_DOUT1 = 0;
			app_write_REG_DOUT1(&app_regs.REG_DOUT1);
			pulse_couter_ms = 0;
		}
	}

	app_regs.REG_DO1_CONF = reg;
	return true;
}


/************************************************************************/
/* REG_DO1_PULSE                                                        */
/************************************************************************/
void app_read_REG_DO1_PULSE(void) {}
bool app_write_REG_DO1_PULSE(void *a)
{
	if (!*((uint8_t*)a))
		return false;

	app_regs.REG_DO1_PULSE = *((uint8_t*)a);
	return true;
}


/************************************************************************/
/* REG_RESERVED8                                                        */
/************************************************************************/
void app_read_REG_RESERVED8(void) {}
bool app_write_REG_RESERVED8(void *a) { app_regs.REG_RESERVED8 = *((uint8_t*)a); return true; }


/************************************************************************/
/* REG_RESERVED9                                                        */
/************************************************************************/
void app_read_REG_RESERVED9(void) {}
bool app_write_REG_RESERVED9(void *a) { return true; }


/************************************************************************/
/* REG_EVNT_ENABLE                                                      */
/************************************************************************/
void app_read_REG_EVNT_ENABLE(void) {}
bool app_write_REG_EVNT_ENABLE(void *a)
{
	app_regs.REG_EVNT_ENABLE = *((uint16_t*)a);
	return true;
}