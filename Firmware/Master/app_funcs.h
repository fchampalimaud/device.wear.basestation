#ifndef _APP_FUNCTIONS_H_
#define _APP_FUNCTIONS_H_
#include <avr/io.h>


/************************************************************************/
/* Define if not defined                                                */
/************************************************************************/
#ifndef bool
	#define bool uint8_t
#endif
#ifndef true
	#define true 1
#endif
#ifndef false
	#define false 0
#endif


/************************************************************************/
/* Prototypes                                                           */
/************************************************************************/
void app_read_REG_START_DATA(void);
void app_read_REG_START_STIM(void);
void app_read_REG_DATA(void);
void app_read_REG_MISC(void);
void app_read_REG_CAM0(void);
void app_read_REG_CAM1(void);
void app_read_REG_DOUT0(void);
void app_read_REG_DOUT1(void);
void app_read_REG_ACQ_STATUS(void);
void app_read_REG_ACQ_EXT_TRIGGER(void);
void app_read_REG_DEV_SELECT(void);
void app_read_REG_TEMP(void);
void app_read_REG_TX_RETRIES(void);
void app_read_REG_BATTERY(void);
void app_read_REG_BATTERY_RAW(void);
void app_read_REG_WEAR_FW_MAJOR(void);
void app_read_REG_WEAR_FW_MINOR(void);
void app_read_REG_WEAR_HW_MAJOR(void);
void app_read_REG_WEAR_HW_MINOR(void);
void app_read_REG_RECEIVER_FW_MAJOR(void);
void app_read_REG_RECEIVER_FW_MINOR(void);
void app_read_REG_RECEIVER_HW_MAJOR(void);
void app_read_REG_RECEIVER_HW_MINOR(void);
void app_read_REG_RX_GOOD(void);
void app_read_REG_RESERVED3(void);
void app_read_REG_CONF_DEV_ID(void);
void app_read_REG_CONF_SAMPLE_FREQ(void);
void app_read_REG_CONF_RANGE_ACC(void);
void app_read_REG_CONF_RANGE_GYR(void);
void app_read_REG_CONF_ENABLE(void);
void app_read_REG_CONF_PRIORITIZATION(void);
void app_read_REG_CONF_IR_LEDS_PERIOD(void);
void app_read_REG_CONF_TX_POWER(void);
void app_read_REG_RESERVED4(void);
void app_read_REG_RESERVED5(void);
void app_read_REG_STIM_FROM_DI0(void);
void app_read_REG_STIM_ON(void);
void app_read_REG_STIM_OFF(void);
void app_read_REG_STIM_REPS(void);
void app_read_REG_STIM_CURRENT(void);
void app_read_REG_RESERVED6(void);
void app_read_REG_RESERVED7(void);
void app_read_REG_MISC_CONF_DI(void);
void app_read_REG_MISC_CONF_RUN(void);
void app_read_REG_CAM0_CONF(void);
void app_read_REG_CAM0_CONF_RUN(void);
void app_read_REG_CAM0_FREQ(void);
void app_read_REG_CAM0_MMODE_PERIOD(void);
void app_read_REG_CAM0_MMODE_PULSE(void);
void app_read_REG_CAM1_CONF(void);
void app_read_REG_CAM1_CONF_RUN(void);
void app_read_REG_CAM1_FREQ(void);
void app_read_REG_CAM1_MMODE_PERIOD(void);
void app_read_REG_CAM1_MMODE_PULSE(void);
void app_read_REG_DO0_CONF(void);
void app_read_REG_DO1_CONF(void);
void app_read_REG_DO1_PULSE(void);
void app_read_REG_RESERVED8(void);
void app_read_REG_RESERVED9(void);
void app_read_REG_EVNT_ENABLE(void);

bool app_write_REG_START_DATA(void *a);
bool app_write_REG_START_STIM(void *a);
bool app_write_REG_DATA(void *a);
bool app_write_REG_MISC(void *a);
bool app_write_REG_CAM0(void *a);
bool app_write_REG_CAM1(void *a);
bool app_write_REG_DOUT0(void *a);
bool app_write_REG_DOUT1(void *a);
bool app_write_REG_ACQ_STATUS(void *a);
bool app_write_REG_ACQ_EXT_TRIGGER(void *a);
bool app_write_REG_DEV_SELECT(void *a);
bool app_write_REG_TEMP(void *a);
bool app_write_REG_TX_RETRIES(void *a);
bool app_write_REG_BATTERY(void *a);
bool app_write_REG_BATTERY_RAW(void *a);
bool app_write_REG_WEAR_FW_MAJOR(void *a);
bool app_write_REG_WEAR_FW_MINOR(void *a);
bool app_write_REG_WEAR_HW_MAJOR(void *a);
bool app_write_REG_WEAR_HW_MINOR(void *a);
bool app_write_REG_RECEIVER_FW_MAJOR(void *a);
bool app_write_REG_RECEIVER_FW_MINOR(void *a);
bool app_write_REG_RECEIVER_HW_MAJOR(void *a);
bool app_write_REG_RECEIVER_HW_MINOR(void *a);
bool app_write_REG_RX_GOOD(void *a);
bool app_write_REG_RESERVED3(void *a);
bool app_write_REG_CONF_DEV_ID(void *a);
bool app_write_REG_CONF_SAMPLE_FREQ(void *a);
bool app_write_REG_CONF_RANGE_ACC(void *a);
bool app_write_REG_CONF_RANGE_GYR(void *a);
bool app_write_REG_CONF_ENABLE(void *a);
bool app_write_REG_CONF_PRIORITIZATION(void *a);
bool app_write_REG_CONF_IR_LEDS_PERIOD(void *a);
bool app_write_REG_CONF_TX_POWER(void *a);
bool app_write_REG_RESERVED4(void *a);
bool app_write_REG_RESERVED5(void *a);
bool app_write_REG_STIM_FROM_DI0(void *a);
bool app_write_REG_STIM_ON(void *a);
bool app_write_REG_STIM_OFF(void *a);
bool app_write_REG_STIM_REPS(void *a);
bool app_write_REG_STIM_CURRENT(void *a);
bool app_write_REG_RESERVED6(void *a);
bool app_write_REG_RESERVED7(void *a);
bool app_write_REG_MISC_CONF_DI(void *a);
bool app_write_REG_MISC_CONF_RUN(void *a);
bool app_write_REG_CAM0_CONF(void *a);
bool app_write_REG_CAM0_CONF_RUN(void *a);
bool app_write_REG_CAM0_FREQ(void *a);
bool app_write_REG_CAM0_MMODE_PERIOD(void *a);
bool app_write_REG_CAM0_MMODE_PULSE(void *a);
bool app_write_REG_CAM1_CONF(void *a);
bool app_write_REG_CAM1_CONF_RUN(void *a);
bool app_write_REG_CAM1_FREQ(void *a);
bool app_write_REG_CAM1_MMODE_PERIOD(void *a);
bool app_write_REG_CAM1_MMODE_PULSE(void *a);
bool app_write_REG_DO0_CONF(void *a);
bool app_write_REG_DO1_CONF(void *a);
bool app_write_REG_DO1_PULSE(void *a);
bool app_write_REG_RESERVED8(void *a);
bool app_write_REG_RESERVED9(void *a);
bool app_write_REG_EVNT_ENABLE(void *a);


#endif /* _APP_FUNCTIONS_H_ */