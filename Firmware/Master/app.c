#include "hwbp_core.h"
#include "hwbp_core_regs.h"
#include "hwbp_core_types.h"

#define F_CPU 32000000
#include <util/delay.h>

#include "app.h"
#include "app_funcs.h"
#include "app_ios_and_regs.h"
#include "protocol.h"
#include "uart1.h"

/************************************************************************/
/* Declare application registers                                        */
/************************************************************************/
extern AppRegs app_regs;
extern uint8_t app_regs_type[];
extern uint16_t app_regs_n_elements[];
extern uint8_t *app_regs_pointer[];
extern void (*app_func_rd_pointer[])(void);
extern bool (*app_func_wr_pointer[])(void*);


/************************************************************************/
/* Initialize app                                                       */
/************************************************************************/
static const uint8_t default_device_name[] = "Wear";

void hwbp_app_initialize(void)
{
    /* Define versions */
    uint8_t hwH = 1;
    uint8_t hwL = 2;
    uint8_t fwH = 1;
    uint8_t fwL = 6;
    uint8_t ass = 0;
    
    /* Start core */
    core_func_start_core(
        1056,
        hwH, hwL,
        fwH, fwL,
        ass,
        (uint8_t*)(&app_regs),
        APP_NBYTES_OF_REG_BANK,
        APP_REGS_ADD_MAX - APP_REGS_ADD_MIN + 1,
        default_device_name
    );
}

/************************************************************************/
/* Handle if a catastrophic error occur                                 */
/************************************************************************/
void core_callback_catastrophic_error_detected(void)
{
	/* Shutdown the 3 sensor devices */
	clr_UART_RF1;
	clr_UART_RF2;
	clr_UART_WIRED;
	clr_EN_RF1;
	clr_EN_RF2;
	clr_EN_WIRED;

	/* Shutdown cameras or motors */
	timer_type0_stop(&TCC0);
	timer_type0_stop(&TCD0);
	clr_CAM0;
	clr_CAM1;
}

/************************************************************************/
/* General definitions                                                  */
/************************************************************************/
// #define NBYTES 23


/************************************************************************/
/* Global variables                                                     */
/************************************************************************/
uint8_t wake_up_counter = 0;
uint16_t wake_up_timeout = 0;
uint8_t select_couter_ms = 0;
uint8_t start_couter_ms = 0;
uint16_t pulse_couter_ms = 0;

#define STATE_STANDBY 0
#define STATE_COMMANDRECEIVED 1
#define STATE_RESETDONE 2
#define STATE_WAITINGFORFIRSTSAMPLE 3
#define STATE_ACQUIRING 4
uint8_t StartState = STATE_STANDBY;

/************************************************************************/
/* General used functions                                               */
/************************************************************************/
wake_up_t wake_up = {CMD_WAKE_UP, 0,0,0,0,0,0,0};
wake_up_t disable_rx = {CMD_DISABLE_RX, 0,0,0,0,0,0,0};

stim1_t stim_type1 = {CMD_STIM_1, 0, 0 ,0 ,0 ,0, 0, 0};
stim2_t stim_type2 = {CMD_STIM_2, 0, 0 ,0 ,0 ,0, 0};
stim3_t stim_type3 = {CMD_STIM_3, 0, 0 ,0 ,0 ,0};

#define WAKE_UP_LEN 8

typedef struct  
{
	bool Temp, Retries, Battery;
	bool WearFwMajor, WearFwMinor, WearHwMajor, WearHwMinor, RxFwMajor, RxFwMinor, RxHwMajor, RxHwMinor;
	bool RxGood;
} SentMetadataEvent_t;

SentMetadataEvent_t SentMetadataEvent;

static uint8_t MetadataCounter;

void fill_wake_up(void)
{
	wake_up.freq = core_bool_is_visual_enabled() ? 0x80 : 0;
	wake_up.freq |= app_regs.REG_CONF_SAMPLE_FREQ | (app_regs.REG_CONF_PRIORITIZATION << 5);
	wake_up.led_txpwr = app_regs.REG_CONF_TX_POWER | (app_regs.REG_CONF_IR_LEDS_PERIOD << 2);
	wake_up.range_xyz = app_regs.REG_CONF_RANGE_ACC;
	wake_up.range_gyr = app_regs.REG_CONF_RANGE_GYR;
	wake_up.id_chmask = *(((uint8_t*)(&app_regs.REG_CONF_DEV_ID)) + 1) << 3;
	wake_up.id_chmask |= app_regs.REG_CONF_ENABLE;
	wake_up.id_LSByte = *((uint8_t*)(&app_regs.REG_CONF_DEV_ID));

	uint8_t Length = 12;

	while(Length--)
	{
		*(((uint8_t*)(&SentMetadataEvent)) + Length) = true;
	}

	SentMetadataEvent.Temp = false;
	SentMetadataEvent.Retries = false;
	SentMetadataEvent.Battery = false;	
	SentMetadataEvent.RxGood = false;

	app_regs.REG_TX_RETRIES = 0;
	MetadataCounter = 0;
}

bool fill_stim (void)
{
    if (app_regs.REG_DEV_SELECT == GM_SEL_WIRED)
    {
        stim_type3.ms_on = app_regs.REG_STIM_ON;
        stim_type3.ms_off = app_regs.REG_STIM_OFF;
        stim_type3.repetitions = app_regs.REG_STIM_REPS;
        stim_type3.current = app_regs.REG_STIM_CURRENT;
    
        stim_type3.checksum = 0;
        for (uint8_t i = LEN_STIM_3 - 1; i != 0; i--)
            stim_type3.checksum += *(((uint8_t *)(&stim_type3)) + i - 1);
        
        return true;
    }
    
    else if (app_regs.REG_DEV_SELECT == GM_SEL_RF1)        
    {
        stim_type2.repetitions = app_regs.REG_STIM_REPS;
        
        if (app_regs.REG_STIM_CURRENT > 255)
            stim_type2.current = 255;
        else
            stim_type2.current = *((uint8_t*)(&app_regs.REG_STIM_CURRENT));
        
        switch (app_regs.REG_CONF_SAMPLE_FREQ)
        {
            case GM_FREQ_50Hz:
                stim_type2.frames_on = app_regs.REG_STIM_ON / 20;
                stim_type2.frames_off = app_regs.REG_STIM_OFF / 20;
                
                if (app_regs.REG_STIM_ON % 20 > 10)
                    stim_type2.frames_on++;
                if (app_regs.REG_STIM_OFF % 20 > 10)
                    stim_type2.frames_off++;
                break;
                    
            case GM_FREQ_100Hz:
                stim_type2.frames_on = app_regs.REG_STIM_ON / 10;
                stim_type2.frames_off = app_regs.REG_STIM_OFF / 10;
                
                if (app_regs.REG_STIM_ON % 10 > 5)
                    stim_type2.frames_on++;
                if (app_regs.REG_STIM_OFF % 10 > 5)
                    stim_type2.frames_off++;
                break;
                
            case GM_FREQ_200Hz:
                stim_type2.frames_on = app_regs.REG_STIM_ON / 5;
                stim_type2.frames_off = app_regs.REG_STIM_OFF / 5;
                
                if (app_regs.REG_STIM_ON % 5 > 3)
                    stim_type2.frames_on++;
                if (app_regs.REG_STIM_OFF % 5 > 3)
                    stim_type2.frames_off++;
                break;
                
            case GM_FREQ_250Hz:
            case GM_FREQ_500Hz:
                break;
        }
        
        stim_type2.checksum = 0;
        for (uint8_t i = LEN_STIM_2 - 1; i != 0; i--)
            stim_type2.checksum += *(((uint8_t *)(&stim_type2)) + i - 1);
        
        return true;
    }
    else
    {
        // To be implemented for RF2
        return false;
    }
    
    return false;
}


/************************************************************************/
/* Initialization Callbacks                                             */
/************************************************************************/
uint16_t AdcOffset;

void core_callback_1st_config_hw_after_boot(void)
{
	/* Initialize IOs */
	init_ios();

	/* Initialize UART for Wired with 500 Kbit/s*/
	uart1_init(3, 0, false);
	uart1_enable();

	/* Initialize ADC */	
	PR_PRPA &= ~(PR_ADC_bm);									// Remove power reduction
	ADCA_CTRLA = ADC_ENABLE_bm;								// Enable ADCA
	ADCA_CTRLB = ADC_CURRLIMIT_HIGH_gc;						// High current limit, max. sampling rate 0.5MSPS
	ADCA_CTRLB  |= ADC_RESOLUTION_12BIT_gc;				// 12-bit result, right adjusted
	ADCA_REFCTRL = ADC_REFSEL_INTVCC_gc;					// VCC/1.6 = 3.3/1.6 = 2.0625 V
	ADCA_PRESCALER = ADC_PRESCALER_DIV128_gc;				// 250 ksps
																		// Propagation Delay = (1 + 12[bits]/2 + 1[gain]) / fADC[125k] = 32 us
																		// Note: For single measurements, Propagation Delay is equal to Conversion Time
		
	ADCA_CH0_CTRL = ADC_CH_INPUTMODE_SINGLEENDED_gc;	// Single-ended positive input signal
	ADCA_CH0_MUXCTRL = 3 << 3;									// Pin 3 for zero calibration
	ADCA_CH0_INTCTRL = ADC_CH_INTMODE_COMPLETE_gc;		// Rise interrupt flag when conversion is complete
	ADCA_CH0_INTCTRL |= ADC_CH_INTLVL_OFF_gc;				// Interrupt is not used
		
	/* Wait 10 us to stabilization before measure the zero/GND value */
	timer_type0_enable(&TCD0, TIMER_PRESCALER_DIV2, 80, INT_LEVEL_OFF);
	while(!timer_type0_get_flag(&TCD0));
	timer_type0_stop(&TCD0);
		
	/* Measure and safe adc offset */
	ADCA_CH0_CTRL |= ADC_CH_START_bm;						// Start conversion
	while(!(ADCA_CH0_INTFLAGS & ADC_CH_CHIF_bm));		// Wait for conversion to finish
	ADCA_CH0_INTFLAGS = ADC_CH_CHIF_bm;						// Clear interrupt bit 
	AdcOffset = ADCA_CH0_RES;									// Read offset

	/* Point ADC to the right channel */
	ADCA_CH0_MUXCTRL = 4 << 3;									// Select pin 4 for further conversions
	ADCA_CH0_CTRL |= ADC_CH_START_bm;						// Force the first conversion	
	while(!(ADCA_CH0_INTFLAGS & ADC_CH_CHIF_bm));		// Wait for conversion to finish
	ADCA_CH0_INTFLAGS = ADC_CH_CHIF_bm;						// Clear interrupt bit
}

void core_callback_reset_registers(void)
{
	/* Execution Control */
	app_regs.REG_START_DATA = 0;
	app_regs.REG_START_STIM = 0;
	app_regs.REG_MISC = 0;
	app_regs.REG_CAM0 = 0;
	app_regs.REG_CAM1 = 0;
	app_regs.REG_DOUT0 = 0;
	app_regs.REG_DOUT1 = 0;
	app_regs.REG_ACQ_STATUS = 0;

	/* MISC control */
	app_regs.REG_MISC_CONF_DI = GM_DI_NOT_USED;
	app_regs.REG_MISC_CONF_RUN = GM_MISC_RUN_STOP;
	
	/* Digital outs control */
	app_regs.REG_DO0_CONF = GM_DO0_REG_DO0;
	app_regs.REG_DO1_CONF = GM_DO1_REG_DO0;
	app_regs.REG_DO1_PULSE = 2;

	/* Cameras */
	app_regs.REG_CAM0_CONF = GM_CAM0_FREQ;
	app_regs.REG_CAM0_CONF_RUN = GM_CAM0_RUN_STOP;
	app_regs.REG_CAM0_FREQ = 25;
	app_regs.REG_CAM0_MMODE_PERIOD = 20000;
	app_regs.REG_CAM0_MMODE_PULSE = 1500;
	app_regs.REG_CAM1_CONF = GM_CAM1_FREQ;
	app_regs.REG_CAM1_CONF_RUN = GM_CAM1_RUN_STOP;
	app_regs.REG_CAM1_FREQ = 25;
	app_regs.REG_CAM1_MMODE_PERIOD = 20000;
	app_regs.REG_CAM1_MMODE_PULSE = 1500;
	
	/* Device selection */
	app_regs.REG_DEV_SELECT = GM_SEL_RF1;

	/* Device configuration */
	app_regs.REG_CONF_DEV_ID = 0;
	app_regs.REG_CONF_SAMPLE_FREQ = GM_FREQ_100Hz;
	app_regs.REG_CONF_RANGE_ACC = GM_ACC_4g;
	app_regs.REG_CONF_RANGE_GYR = GM_GYR_1000dps;	
	app_regs.REG_CONF_ENABLE = B_USE_ACC | B_USE_GYR;
	app_regs.REG_CONF_PRIORITIZATION = GM_PRIO_AVERAGE;
	app_regs.REG_CONF_IR_LEDS_PERIOD = GM_IR_0ms;
	app_regs.REG_CONF_TX_POWER = GM_TX_0dBm;
	
	/* Stimulation */
	app_regs.REG_STIM_FROM_DI0 = GM_FROM_NOT_USED;	// Software only
	app_regs.REG_STIM_ON = 50;
	app_regs.REG_STIM_OFF = 50;
	app_regs.REG_STIM_REPS = 10;
	app_regs.REG_STIM_CURRENT = 10;

	/* Events */
	app_regs.REG_EVNT_ENABLE = B_EVT7 | B_EVT6 | B_EVT5 | B_EVT4 | B_EVT3 | B_EVT2 | B_EVT1 | B_EVT0;
}

void core_callback_registers_were_reinitialized(void)
{
	/* Execution Control */
	app_regs.REG_START_DATA = 0;	// Force ZERO
	app_regs.REG_START_STIM = 0;	// Force ZERO
	app_read_REG_MISC();
	app_regs.REG_ACQ_STATUS = 0;
	app_write_REG_CAM0(&app_regs.REG_CAM0);
	app_write_REG_CAM1(&app_regs.REG_CAM1);	
	app_write_REG_DOUT0(&app_regs.REG_DOUT0);
	app_write_REG_DOUT1(&app_regs.REG_DOUT1);
	pulse_couter_ms = 0;
	
	/* MISC control */
	app_write_REG_MISC_CONF_DI(&app_regs.REG_MISC_CONF_DI);

	/* Digital outs control */
	// Nothing to do!

	/* Cameras */
	uint8_t aux;
	aux = app_regs.REG_CAM0_CONF;
	app_regs.REG_CAM0_CONF = 0xFF;
	app_write_REG_CAM0_CONF(&aux);
	aux = app_regs.REG_CAM1_CONF;
	app_regs.REG_CAM1_CONF = 0xFF;
	app_write_REG_CAM1_CONF(&aux);
   
   clr_UART_RF1;
   clr_UART_RF2;
   clr_UART_WIRED;
   clr_EN_RF1;
   clr_EN_RF2;
   clr_EN_WIRED;
   
   /* Reset RF1 */
   /* This is correcting the issue when the device resets trough software and other 
      device starts straming to a different basestation.
   */
   _delay_ms(100);      // 1 ms is enough, but I'm using 100 because I don't know exactly how this solves the issue
   set_UART_RF1;
   _delay_ms(100);
   clr_UART_RF1;

	/* Device selection */
	app_write_REG_DEV_SELECT(&app_regs.REG_DEV_SELECT);		// Will update the necessary pins

	/* Device configuration */

}

/************************************************************************/
/* Callbacks: Visualization                                             */
/************************************************************************/
void core_callback_visualen_to_on(void)
{
	/* Update channels enable indicators */
	//update_enabled_pwmx();
}

void core_callback_visualen_to_off(void)
{
	/* Clear all the enabled indicators */
}

/************************************************************************/
/* Callbacks: Change on the operation mode                              */
/************************************************************************/
bool stop_state_after_go_to_active = true;

void core_callback_device_to_standby(void)
{
    uint8_t aux = 0;
    stop_state_after_go_to_active = app_write_REG_START_DATA(&aux);
}

void core_callback_device_to_active(void) {}
void core_callback_device_to_enchanced_active(void) {}
void core_callback_device_to_speed(void) {}

/************************************************************************/
/* Callbacks: 1 ms timer                                                */
/************************************************************************/
uint16_t second_counter = 0;

void core_callback_t_before_exec(void)
{
    if (++second_counter == 2000)
    {
        if (StartState == STATE_ACQUIRING)
        {
            if (app_regs.REG_DO0_CONF == GM_DO0_DATA_SEC)
            {
                app_regs.REG_DOUT0 ^= 1;
                app_write_REG_DOUT0(&app_regs.REG_DOUT0);
                
                if (app_regs.REG_EVNT_ENABLE & B_EVT5)
                {
                    core_func_send_event(ADD_REG_DOUT0, true);
                }
            }
        }
    }
}
void core_callback_t_after_exec(void) {}

extern uint8_t stop_timeout_s;
void core_callback_t_new_second(void)
{
	second_counter = 0;
    
    /* Reset authorization to send metadata Events */
	SentMetadataEvent.Temp = true;
	SentMetadataEvent.Retries = true;
	SentMetadataEvent.RxGood = true;

	/* Each 4 seconds */
	if ((MetadataCounter++ & 3) == 3)
	{		
		SentMetadataEvent.Battery = true;
	}
    
    /* Try again to stop the sensor device */
    if (!stop_state_after_go_to_active)
    {
        uint8_t aux = 0;
        stop_state_after_go_to_active = app_write_REG_START_DATA(&aux);
    }
    
    /* Check stop acquisition timeout */
    if (stop_timeout_s > 0)
    {
        if (!(--stop_timeout_s))
        {
            /* timeout achieved */
            if (StartState == STATE_ACQUIRING)
            {
                uint8_t aux = 0;
                stop_state_after_go_to_active = app_write_REG_START_DATA(&aux);
            }
        }
    }
}

extern void ProcessDataFrame(void);
extern void send_stimulation_command(void);

void core_callback_t_500us(void)
{    
    ProcessDataFrame();
    
   /* Pulse */
   if (pulse_couter_ms)
   {
      pulse_couter_ms--;

      if (pulse_couter_ms == 0)
      {
         app_regs.REG_DOUT1 = 0;
         app_write_REG_DOUT1(&app_regs.REG_DOUT1);
         pulse_couter_ms = 0;

         if (app_regs.REG_EVNT_ENABLE & B_EVT5)
         {
            core_func_send_event(ADD_REG_DOUT1, true);
         }
      }
   }
}

void core_callback_t_1ms(void)
{
    ProcessDataFrame();
	
	/* Start data acquisition */
	if (wake_up_counter)
	{
		if ((StartState == STATE_COMMANDRECEIVED) || (StartState == STATE_RESETDONE))
		{
			wake_up_counter--;
		
			if (wake_up_counter == 95)
			{
				if (app_regs.REG_DEV_SELECT == GM_SEL_RF1)
				{
					//set_RST_RF1;
					set_EN_RF1;
				}

				StartState = STATE_RESETDONE;			
			}

			if (wake_up_counter == 0)
			{
				fill_wake_up();
				uart1_xmit((uint8_t*)(&wake_up), LEN_DATA_16BITs);

				StartState = STATE_WAITINGFORFIRSTSAMPLE;
			}
		}
	}

	/* Start up timeout */
	if (wake_up_timeout)
	{
		--wake_up_timeout;

		if ((wake_up_timeout == 0) && (StartState == STATE_WAITINGFORFIRSTSAMPLE))
		{
			StartState = STATE_STANDBY;
			app_regs.REG_START_DATA = 0;
		}
	}
	
	/* De-bounce START button */
	if (start_couter_ms)
	{
		if (!(read_SW_START))
		{
			if (!--start_couter_ms)
			{
				uint8_t aux = 1;
				app_write_REG_START_DATA(&aux);
			}
		}
		else
		{
			start_couter_ms = 0;
		}
	}

	/* De-bounce SELECT/STOP button */
	if (select_couter_ms)
	{
		if (!(read_SW_STOP_SELECT))
		{
			if (!--select_couter_ms)
			{
				if (StartState == STATE_ACQUIRING)
				{
					uint8_t aux = 0;
					app_write_REG_START_DATA(&aux);
				}
				else if (StartState == STATE_STANDBY)
				{
					uint8_t reg;

					if(app_regs.REG_DEV_SELECT == GM_SEL_RF1) { reg = GM_SEL_RF2; }
					if(app_regs.REG_DEV_SELECT == GM_SEL_RF2) { reg = GM_SEL_WIRED; }
					if(app_regs.REG_DEV_SELECT == GM_SEL_WIRED) { reg = GM_SEL_RF1; }

					app_write_REG_DEV_SELECT(&reg);

					if(app_regs.REG_EVNT_ENABLE & B_EVT0)
					{
						core_func_send_event(ADD_REG_DEV_SELECT, true);
					}
				}
			}
		}
		else
		{
			select_couter_ms = 0;
		}
	}			
}

/************************************************************************/
/* Callbacks: uart control                                              */
/************************************************************************/
//#define LoadCtsState CtsState = UART1_CTS_PORT.IN; disable_uart1_rx
//#define RestoreCtsState if (!(CtsState & (1 << UART1_CTS_pin))) enable_uart1_rx;		// Enable only if previously enabled

void core_callback_uart_rx_before_exec(void)  { /* set_io(PORTC, 4); LoadCtsState; */}
void core_callback_uart_rx_after_exec(void)   { /* RestoreCtsState; clear_io(PORTC, 4); */}
void core_callback_uart_tx_before_exec(void)  { /* set_io(PORTC, 4); LoadCtsState; */}
void core_callback_uart_tx_after_exec(void)   { /* RestoreCtsState; clear_io(PORTC, 4); */}
void core_callback_uart_cts_before_exec(void) { /* set_io(PORTC, 4); LoadCtsState; */}
void core_callback_uart_cts_after_exec(void)  { /* RestoreCtsState; clear_io(PORTC, 4); */}

/************************************************************************/
/* Callbacks: Read app register                                         */
/************************************************************************/
bool core_read_app_register(uint8_t add, uint8_t type)
{
	/* Check if it will not access forbidden memory */
	if (add < APP_REGS_ADD_MIN || add > APP_REGS_ADD_MAX)
		return false;
	
	/* Check if type matches */
	if (app_regs_type[add-APP_REGS_ADD_MIN] != type)
		return false;
	
	/* Receive data */
	(*app_func_rd_pointer[add-APP_REGS_ADD_MIN])();	

	/* Return success */
	return true;
}

/************************************************************************/
/* Callbacks: Write app register                                        */
/************************************************************************/
bool core_write_app_register(uint8_t add, uint8_t type, uint8_t * content, uint16_t n_elements)
{
	/* Check if it will not access forbidden memory */
	if (add < APP_REGS_ADD_MIN || add > APP_REGS_ADD_MAX)
		return false;
	
	/* Check if type matches */
	if (app_regs_type[add-APP_REGS_ADD_MIN] != type)
		return false;

	/* Check if the number of elements matches */
	if (app_regs_n_elements[add-APP_REGS_ADD_MIN] != n_elements)
		return false;

	/* Process data and return false if write is not allowed or contains errors */
	return (*app_func_wr_pointer[add-APP_REGS_ADD_MIN])(content);
}