#include "cpu.h"
#include "hwbp_core_types.h"
#include "app_ios_and_regs.h"
#include "app_funcs.h"
#include "hwbp_core.h"
#include "uart1.h"
#include "protocol.h"

#define F_CPU 32000000
#include <util/delay.h>

extern uint16_t pulse_couter_ms;

/************************************************************************/
/* Declare application registers                                        */
/************************************************************************/
extern AppRegs app_regs;

/************************************************************************/
/* Interrupts from Timers                                               */
/************************************************************************/
// ISR(TCC0_OVF_vect, ISR_NAKED)
// ISR(TCD0_OVF_vect, ISR_NAKED)
// ISR(TCE0_OVF_vect, ISR_NAKED)
// ISR(TCF0_OVF_vect, ISR_NAKED)
// 
// ISR(TCC0_CCA_vect, ISR_NAKED)
// ISR(TCD0_CCA_vect, ISR_NAKED)
// ISR(TCE0_CCA_vect, ISR_NAKED)
// ISR(TCF0_CCA_vect, ISR_NAKED)
// 
// ISR(TCD1_OVF_vect, ISR_NAKED)
// 
// ISR(TCD1_CCA_vect, ISR_NAKED)


/************************************************************************/
/* Send stimulation command                                             */
/************************************************************************/
uint8_t send_stim_cmd_type2_counter;

extern bool stim_was_sent;
extern stim2_t stim_type2;

ISR(TCE0_OVF_vect, ISR_NAKED)
{
    send_stim_cmd_type2_counter++;
    uart1_xmit(((uint8_t*)(&stim_type2)) + send_stim_cmd_type2_counter, 1);
    
    //timer_type0_enable(&TCE0, TIMER_PRESCALER_DIV256, 32, INT_LEVEL_LOW);   // 256 us
    
    if (send_stim_cmd_type2_counter == LEN_STIM_2 - 1)
    {
        stim_was_sent = true;        
        timer_type0_stop(&TCE0);
    }
    
    reti();
}


/************************************************************************/
/* Camera 0                                                             */
/************************************************************************/
extern bool stop_camera0;

ISR(TCD0_OVF_vect, ISR_NAKED)
{
	app_regs.REG_CAM0 = B_CAM0;

	if (app_regs.REG_EVNT_ENABLE & B_EVT3)
	{
		core_func_send_event(ADD_REG_CAM0, true);
	}

	reti();
}

ISR(TCD0_CCA_vect, ISR_NAKED)
{
	app_regs.REG_CAM0 = 0;
    
    if (stop_camera0)
    {
        stop_camera0 = false;
        
        clr_CAM0;
        timer_type0_stop(&TCD0);
    }

	reti();
}

/************************************************************************/
/* Camera 1                                                             */
/************************************************************************/
extern bool stop_camera1;

ISR(TCC0_OVF_vect, ISR_NAKED)
{
	app_regs.REG_CAM1 = B_CAM1;

	if (app_regs.REG_EVNT_ENABLE & B_EVT4)
	{
		core_func_send_event(ADD_REG_CAM1, true);
	}

	reti();
}

ISR(TCC0_CCA_vect, ISR_NAKED)
{
	app_regs.REG_CAM1 = 0;

    if (stop_camera1)
    {
        stop_camera1 = false;
        
        clr_CAM1;
        timer_type0_stop(&TCC0);
    }
    
	reti();
}


/************************************************************************/
/* Check and send MISC                                                  */
/************************************************************************/
void ProcessMiscEvent(void)
{
	if ((app_regs.REG_MISC_CONF_RUN == GM_MISC_RUN_RUN) || ((app_regs.REG_MISC_CONF_RUN == GM_MISC_RUN_DATA) && (app_regs.REG_ACQ_STATUS == B_STATUS)))
	{
		app_read_REG_MISC();

		if (app_regs.REG_EVNT_ENABLE & B_EVT2)
		{
			core_func_send_event(ADD_REG_MISC, true);
		}
	}
}

/************************************************************************/
/* MISC timer                                                           */
/************************************************************************/
ISR(TCD1_OVF_vect, ISR_NAKED)
{
	ProcessMiscEvent();
	reti();
}

/************************************************************************/ 
/* SAMPLE_WIRED                                                         */
/* SAMPLE_RF1                                                           */
/* SAMPLE_RF2                                                           */
/************************************************************************/
timestamp_t FrameTimestamp;
bool FrameTimestampAvailable = false;
bool DataFrameAvailable = false;

uint8_t stop_timeout_s = 0;

#define STATE_STANDBY 0
#define STATE_COMMANDRECEIVED 1
#define STATE_RESETDONE 2
#define STATE_WAITINGFORFIRSTSAMPLE 3
#define STATE_ACQUIRING 4
extern uint8_t StartState;

extern void start_cam0 (void);
extern void start_cam1 (void);

ISR(PORTB_INT0_vect, ISR_NAKED)
{
	if (StartState < STATE_WAITINGFORFIRSTSAMPLE)
		reti();

	if (!DataFrameAvailable)
	{
		core_func_mark_user_timestamp();
	}
	else
	{
		FrameTimestamp.second = core_func_read_R_TIMESTAMP_SECOND();
		FrameTimestamp.usecond = core_func_read_R_TIMESTAMP_MICRO();
		FrameTimestampAvailable = true;
	}

	if (StartState == STATE_WAITINGFORFIRSTSAMPLE)
	{
		app_regs.REG_ACQ_STATUS = B_STATUS;

		if (app_regs.REG_EVNT_ENABLE & B_EVT0)
		{
			core_func_send_event(ADD_REG_ACQ_STATUS, true);
		}

		StartState = STATE_ACQUIRING;
        stop_timeout_s = 11 + 1;    // Start timeout equal to 11 seconds

		if (app_regs.REG_DO0_CONF == GM_DO0_START)
		{
			app_regs.REG_DOUT0 = 1;
			app_write_REG_DOUT0(&app_regs.REG_DOUT0);
		}

		if (app_regs.REG_DO1_CONF == GM_DO1_START)
		{
			app_regs.REG_DOUT1 = 1;
			app_write_REG_DOUT1(&app_regs.REG_DOUT1);
			pulse_couter_ms = 0;
		}

		if (app_regs.REG_CAM0_CONF == GM_CAM0_FREQ && app_regs.REG_CAM0_CONF_RUN == GM_CAM0_RUN_DATA)
		{
            clr_CAM0;
            _delay_us(16);      // Measured, gives around 55us before the first trigger pulse
			start_cam0();
            stop_camera0 = false;
		}

		if (app_regs.REG_CAM1_CONF == GM_CAM1_FREQ && app_regs.REG_CAM1_CONF_RUN == GM_CAM1_RUN_DATA)
		{
            clr_CAM1;
            _delay_us(16);      // Measured, gives around 55us before the first trigger pulse
			start_cam1();
            stop_camera1 = false;
		}
			
		/* Start or stop inputs if configured that way */
		app_write_REG_MISC_CONF_DI(&app_regs.REG_MISC_CONF_DI);
	}
	
	
	if ((app_regs.REG_MISC_CONF_RUN != GM_MISC_RUN_STOP) && (app_regs.REG_MISC_CONF_DI == GM_FREQ_DATA))
	{
		app_read_REG_MISC();

		if (app_regs.REG_EVNT_ENABLE & B_EVT2)
		{
			core_func_send_event(ADD_REG_MISC, true);
		}
	}
	
	reti();
}

/************************************************************************/ 
/* DI0                                                                  */
/************************************************************************/
static uint8_t auxBit;
ISR(PORTC_INT1_vect, ISR_NAKED)
{
   if (read_DI0)
   {
      if (app_regs.REG_RESERVED8 == 1)
      {
         app_regs.REG_RESERVED8 = 0;
         
         auxBit = B_DO1;
         app_write_REG_DOUT1(&auxBit);  
         
      }
   }
   else
   {
      if (app_regs.REG_RESERVED8 == 2)
      {
         app_regs.REG_RESERVED8 = 0;
         
         auxBit = B_DO1;
         app_write_REG_DOUT1(&auxBit);
      }
   }      
     
	if (read_DI0)
	{
		if (app_regs.REG_MISC_CONF_DI == GM_DI0_RISING)
		{
			ProcessMiscEvent();
		}
		
		if ((app_regs.REG_ACQ_EXT_TRIGGER & MSK_EXT_CONF) == GM_EXT_DI0)
		{
			auxBit = 1;
			app_write_REG_START_DATA(&auxBit);
		}

        if ((app_regs.REG_STIM_FROM_DI0 & MSK_FROM_DI_CONF) == GM_FROM_DI0_RISING)
		{
			auxBit = 1;
			app_write_REG_START_STIM(&auxBit);
		}
	}
	else
	{
		if (app_regs.REG_MISC_CONF_DI == GM_DI0_FALLING)
		{
			ProcessMiscEvent();
		}
		
		if ((app_regs.REG_ACQ_EXT_TRIGGER & MSK_EXT_CONF) == GM_EXT_DI0)
		{
			auxBit = 0;
			app_write_REG_START_DATA(&auxBit);
		}

        if ((app_regs.REG_STIM_FROM_DI0 & MSK_FROM_DI_CONF) == GM_FROM_DI0_FALLING)
		{
			auxBit = 1;
			app_write_REG_START_STIM(&auxBit);
		}
	}

	if (app_regs.REG_MISC_CONF_DI == GM_DI0_RISE_AND_FALL)
	{
		ProcessMiscEvent();
	}

	reti();
}

/************************************************************************/ 
/* DI1                                                                  */
/************************************************************************/
ISR(PORTA_INT1_vect, ISR_NAKED)
{
	if (read_DI1)
	{
		if (app_regs.REG_MISC_CONF_DI == GM_DI1_RISING)
		{
			ProcessMiscEvent();
		}
		
		if ((app_regs.REG_ACQ_EXT_TRIGGER & MSK_EXT_CONF) == GM_EXT_DI1)
		{
			auxBit = 1;
			app_write_REG_START_DATA(&auxBit);
		}

        if ((app_regs.REG_STIM_FROM_DI0 & MSK_FROM_DI_CONF) == GM_FROM_DI1_RISING)
		{
			auxBit = 1;
			app_write_REG_START_STIM(&auxBit);
		}
	}
	else
	{
		if (app_regs.REG_MISC_CONF_DI == GM_DI1_FALLING)
		{
			ProcessMiscEvent();
		}
		
		if ((app_regs.REG_ACQ_EXT_TRIGGER & MSK_EXT_CONF) == GM_EXT_DI1)
		{
			auxBit = 0;
			app_write_REG_START_DATA(&auxBit);
		}

        if ((app_regs.REG_STIM_FROM_DI0 & MSK_FROM_DI_CONF) == GM_FROM_DI1_FALLING)
		{
			auxBit = 1;
			app_write_REG_START_STIM(&auxBit);
		}
	}

	if (app_regs.REG_MISC_CONF_DI == GM_DI1_RISE_AND_FALL)
	{
		ProcessMiscEvent();
	}

	reti();
}

/************************************************************************/ 
/* SW_START                                                             */
/************************************************************************/
extern uint8_t start_couter_ms;

ISR(PORTD_INT1_vect, ISR_NAKED)
{
	start_couter_ms = 25;
	reti();
}

/************************************************************************/ 
/* SW_STOP_SELECT                                                       */
/************************************************************************/
extern uint8_t select_couter_ms;

ISR(PORTB_INT1_vect, ISR_NAKED)
{
	select_couter_ms =  25;
	reti();
}

/************************************************************************/
/* Process BYTE RECEIVED                                                */
/************************************************************************/
static uint8_t pointer = 0;
static data_16_bits_t dataIn;
static uint8_t * dataInPointer;

static uint8_t stim_ack_counter = 0;

extern bool stim_was_sent;

static uint8_t checksum;

void ProcessDataFrame(void);

void uart1_rcv_byte_callback(uint8_t byte)
{
	if (pointer > 2)
	{
		if (pointer < LEN_DATA_16BITs + 3)
		{
			*( ((uint8_t*)(&dataIn)) - 3 + pointer++) = byte;
		}
		else
		{
			checksum = 0;			
			for (uint8_t i = LEN_DATA_16BITs; i != 0; i--)
				checksum += *(((uint8_t *)(&dataIn)) + i - 1);
			
			pointer = 0;
			
			if (checksum == byte)
			{
				if (dataIn.id == *((uint8_t*)(&app_regs.REG_CONF_DEV_ID)) || app_regs.REG_DEV_SELECT == GM_SEL_WIRED)
				{
					DataFrameAvailable = true;
					disable_uart1_rx;
                    
                    if (app_regs.REG_DO0_CONF == GM_DO0_DATA)
                    {
                        app_regs.REG_DOUT0 ^= 1;
                        app_write_REG_DOUT0(&app_regs.REG_DOUT0);
                    }

                    if (app_regs.REG_DO1_CONF == GM_DO1_DATA)
                    {
                        app_regs.REG_DOUT1 ^= 1;
                        app_write_REG_DOUT1(&app_regs.REG_DOUT1);
                        pulse_couter_ms = 0;
                    }
				}
		    }
		    else
		    {
			    dataInPointer = (uint8_t*)(&dataIn);
			
			    if (byte == 'w')
			    {
				    pointer = 1;
			    }
			    else if ((byte == '>') && (*(dataInPointer+LEN_DATA_16BITs-1) == 'w'))
			    {
				    pointer = 2;
			    }
			    else if ((byte == ':') && (*(dataInPointer+LEN_DATA_16BITs-1) == '>') && (*(dataInPointer+LEN_DATA_16BITs-2) == 'w'))
			    {
				    pointer = 3;
			    }
		    }
		}
	}
	else if (pointer == 0)
	{
		if (byte == 'w')
		{
			pointer++;
		}
	}
	else if (pointer == 1)
	{
		if (byte == '>')
		{
			pointer++;
		}
		else
		{
			pointer = 0;
		}
	}
	else if (pointer == 2)
	{
		if (byte == ':')
		{
			pointer++;
		}
		else
		{
			pointer = 0;     
		}
	}    
    
    if (stim_was_sent)
    {
        if (byte == 'a')
        {        
            if (++stim_ack_counter == 3)
            {
                stim_ack_counter = 0;
                stim_was_sent = false;
            
                if (app_regs.REG_EVNT_ENABLE & B_EVT0)
                {
                    app_regs.REG_START_STIM = 1;
                    core_func_send_event(ADD_REG_START_STIM, true);
                    app_regs.REG_START_STIM = 0;
                }   
            }            
        }        
        else
        {
            stim_ack_counter = 0;
        }
    }        
}

/************************************************************************/
/* Process Stimulation Command                                          */
/************************************************************************/


/************************************************************************/
/* Process Data Frame                                                   */
/************************************************************************/
typedef struct
{
	bool Temp, Retries, Battery;
	bool WearFwMajor, WearFwMinor, WearHwMajor, WearHwMinor, RxFwMajor, RxFwMinor, RxHwMajor, RxHwMinor;
	bool RxGood;
} SentMetadataEvent_t;

extern SentMetadataEvent_t SentMetadataEvent;

extern uint16_t TxRetries;

void ProcessMetadata(void);

uint8_t first_packets = 0;

void ProcessDataFrame(void)
{
	if (DataFrameAvailable)
	{
		DataFrameAvailable = false;
        stop_timeout_s = 11 + 1;    // Restart timeout equal to 11 seconds

		uint8_t *psource; 
		uint8_t *pdest; 
		uint8_t size; 

		psource = (uint8_t*)(&dataIn.data_sensor1); 
		pdest = (uint8_t*)(&app_regs.REG_DATA);
		size = 18;

		while(size--)
		{
			*pdest++ = *psource++;
		}
		
		uint8_t aux;
		size = 9;

		while(size--)
		{
			aux = *(((uint8_t*)(app_regs.REG_DATA)) + size*2 + 1);
			*(((uint8_t*)(app_regs.REG_DATA)) + size*2 + 1) = *(((uint8_t*)(app_regs.REG_DATA)) + size*2);
			*(((uint8_t*)(app_regs.REG_DATA)) + size*2) = aux;
		}		
	    
        /* Update stim notification only if using RF1 */
        /* This notification is already implemented on the Wired code but it's not used
           since the REG_START_STIM event is very precise (delay of around 80 us typical) */
        if (app_regs.REG_DEV_SELECT == GM_SEL_RF1)
        {
            app_regs.REG_DATA[9] = (dataIn.counter & 0x80) ? 1000 : 0;
            app_regs.REG_DATA[9] += dataIn.counter & 0x7F;
        }
        else
        {
            app_regs.REG_DATA[9] = dataIn.counter & 0x7F;
        }
       
        if (first_packets == 0)
        {
		      uint8_t ok = 0;
            
            // Check first 6 bytes
            for (uint8_t i = 0; i < 6; i++)
               if (*(((uint8_t*)(&app_regs.REG_DATA[0])) + i) != app_regs.REG_CONF_DEV_ID)
                  ok++;             
               
            if (ok)
               if (app_regs.REG_EVNT_ENABLE & B_EVT1)
                  core_func_send_event(ADD_REG_DATA, false);
        }
        else
        {
            first_packets--;
        }
        
        ProcessMetadata();

		if (FrameTimestampAvailable)
		{
			FrameTimestampAvailable = false;
			core_func_update_user_timestamp(FrameTimestamp.second, FrameTimestamp.usecond);
		}

		enable_uart1_rx;
	}
}

bool first_tx_retries = true;

void ProcessMetadata(void)
{
	if (dataIn.cmd >= 0x0E && dataIn.cmd <= 0x15)
	{
		*(((uint8_t*)(&app_regs.REG_WEAR_FW_MAJOR)) + (dataIn.cmd - 0x0E)) = dataIn.metadata;
	
		if (*(((bool*)(&SentMetadataEvent.WearFwMajor)) + (dataIn.cmd - 0x0E)))
		{
			*(((bool*)(&SentMetadataEvent.WearFwMajor)) + (dataIn.cmd - 0x0E)) = false;

			if (app_regs.REG_EVNT_ENABLE & B_EVT7)
			{
				core_func_send_event(ADD_REG_WEAR_FW_MAJOR + (dataIn.cmd - 0x0E), false);
			}
		}
	}
	else
	{
		switch (dataIn.cmd)
		{
			case 0x00:
				if (first_tx_retries)
                {
                    first_tx_retries = false;                    
                    app_regs.REG_TX_RETRIES = 0;
                    dataIn.metadata = 0;
                }
                
                app_regs.REG_TX_RETRIES += dataIn.metadata;
                                    
				if (SentMetadataEvent.Retries)
				{
					SentMetadataEvent.Retries = false;

					if (app_regs.REG_EVNT_ENABLE & B_EVT6)
					{
						core_func_send_event(ADD_REG_TX_RETRIES, false);
						app_regs.REG_TX_RETRIES = 0;
					}
				}
			
			break;
			
			case 0x02:
				app_regs.REG_RX_GOOD = dataIn.metadata;
			
				if (SentMetadataEvent.RxGood)
				{
					SentMetadataEvent.RxGood = false;

					if (app_regs.REG_EVNT_ENABLE & B_EVT6)
					{
						core_func_send_event(ADD_REG_RX_GOOD, false);
					}
				}

				break;
			
			case 0x03:
				app_regs.REG_TEMP = dataIn.metadata;
			
				if (SentMetadataEvent.Temp)
				{
					SentMetadataEvent.Temp = false;

					if (app_regs.REG_EVNT_ENABLE & B_EVT6)
					{
						core_func_send_event(ADD_REG_TEMP, false);
					}
				}

				break;

			case 0x0B:
				app_regs.REG_BATTERY = dataIn.metadata;
			
				if (SentMetadataEvent.Battery)
				{
					SentMetadataEvent.Battery = false;

					if (app_regs.REG_EVNT_ENABLE & B_EVT6)
					{
						core_func_send_event(ADD_REG_BATTERY, false);
					}
				}

				break;
            
            case 0x0C:
                break;
                
			case 0x0D:
				app_regs.REG_BATTERY_RAW = dataIn.metadata;

				break;
		}
	}
}